package fr.soleil.comete.bean.trend.model;

public class AttributeName {

    protected final String deviceName, attributeName, fullName;

    public AttributeName(String deviceName, String attributeName, String fullName) {
        super();
        this.deviceName = deviceName;
        this.attributeName = attributeName;
        this.fullName = fullName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public String toString() {
        return getAttributeName();
    }

}
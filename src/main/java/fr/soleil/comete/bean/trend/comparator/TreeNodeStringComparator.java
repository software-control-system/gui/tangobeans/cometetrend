package fr.soleil.comete.bean.trend.comparator;

import java.text.Collator;
import java.util.Comparator;

import javax.swing.tree.DefaultMutableTreeNode;

public class TreeNodeStringComparator implements Comparator<DefaultMutableTreeNode> {

    private final Collator comparator;

    public TreeNodeStringComparator() {
        super();
        comparator = Collator.getInstance();
    }

    @Override
    public int compare(DefaultMutableTreeNode n1, DefaultMutableTreeNode n2) {
        int result;
        if (n1 == null) {
            if (n2 == null) {
                result = 0;
            } else {
                result = -1;
            }
        } else if (n2 == null) {
            result = 1;
        } else {
            Object o1 = n1.getUserObject(), o2 = n2.getUserObject();
            if (o1 == null) {
                if (o2 == null) {
                    result = 0;
                } else {
                    result = -1;
                }
            } else if (o2 == null) {
                result = 1;
            } else {
                result = comparator.compare(o1.toString(), o2.toString());
            }
        }
        return result;
    }

}

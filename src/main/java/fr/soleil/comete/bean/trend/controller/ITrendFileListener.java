package fr.soleil.comete.bean.trend.controller;

public interface ITrendFileListener {

    public void trendFileEvent(final TrendEvent event);

}

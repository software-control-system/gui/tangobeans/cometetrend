package fr.soleil.comete.bean.trend.view;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;

import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.lib.project.swing.icons.DecorableIcon;

public class ResourcesUtil {

    public static final ResourceBundle MESSAGES = ResourceBundle.getBundle("fr.soleil.comete.bean.trend.messages");
    public static final ResourceBundle RESOURCES = ResourceBundle.getBundle("fr.soleil.comete.bean.trend.icons");

    public static final String ROOT_NODE = ResourcesUtil.MESSAGES.getString("CometeTrend.Tree.Root");

    public static CometeImage getImage(String key) {
        URL file = ResourcesUtil.class.getResource(RESOURCES.getString(key));
        return new CometeImage(file.toString());
    }

    public static ImageIcon getIcon(String key) {
        URL file = ResourcesUtil.class.getResource(RESOURCES.getString(key));
        ImageIcon icon = new ImageIcon(file);
        return icon;
    }

    public static DecorableIcon getDecorableIcon(String mainKey, String decorableKey) {
        URL mainURL = ResourcesUtil.class.getResource(RESOURCES.getString(mainKey));
        URL decoratedURL = ResourcesUtil.class.getResource(RESOURCES.getString(decorableKey));
        DecorableIcon icon = new DecorableIcon(mainURL);
        icon.setDecoration(new ImageIcon(decoratedURL));
        icon.setDecorated(true);
        return icon;
    }

    public static String getFileJarName(final Class<?> clazz) {
        String jarFileName = getCompleteFileJarName(clazz);
        if ((jarFileName != null) && !jarFileName.isEmpty()) {
            int indexEnd = jarFileName.indexOf("-");
            if (indexEnd > -1) {
                jarFileName = jarFileName.substring(0, indexEnd);
            }
        }
        return jarFileName;
    }

    private static String getCompleteFileJarName(Class<?> clazz) {
        String completeJarFilename = null;
        if (clazz != null) {
            CodeSource source = clazz.getProtectionDomain().getCodeSource();
            URL location = source.getLocation();
            String path = location.getPath();

            try {
                String decodedPath = URLDecoder.decode(path, "UTF-8");
                File file = new File(decodedPath);
                String name = file.getName();

                if (name.endsWith(".jar")) {
                    completeJarFilename = name;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return completeJarFilename;
    }

    public static String getFileJarVersion(Class<?> clazz) {
        String version = null;
        if (clazz != null) {
            String jarFileName = getCompleteFileJarName(clazz);
            if ((jarFileName != null) && !jarFileName.isEmpty()) {
                int indexEnd = jarFileName.lastIndexOf(".jar");
                int indexBegin = jarFileName.lastIndexOf("-");
                if ((indexEnd > -1) && (indexBegin > -1)) {
                    version = jarFileName.substring(indexBegin + 1, indexEnd);
                }
            }
        }
        return version;
    }
}

package fr.soleil.comete.bean.trend.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.ref.WeakReference;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.comete.bean.trend.action.TrendFrequencyAction;
import fr.soleil.comete.bean.trend.comparator.TreeNodeStringComparator;
import fr.soleil.comete.bean.trend.controller.CometeTrendController;
import fr.soleil.comete.bean.trend.controller.ITrendFileListener;
import fr.soleil.comete.bean.trend.controller.TrendEvent;
import fr.soleil.comete.bean.trend.controller.TrendEvent.EventType;
import fr.soleil.comete.bean.trend.model.TrendFile;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;
import fr.soleil.lib.project.swing.model.SortTreeModel;

public class TrendMenu implements ITrendFileListener {

    private static final String TANGO_HOST = "TANGO_HOST";

    private final CometeTrendController controller;
    private final WeakReference<ChartPanel> chartPanelReference;
    private final WeakReference<CometeTrend> parentReference;

    private AboutDialog helpDialog;
    private JFrame parentFrame;

    private final JMenuBar menuBar;
    private final JMenu menuFile;
    private final JMenu menuConfiguration;
    private final JMenu menuMonitoring;
    private final JMenu menuHelp;
    private final JMenuItem aboutMenu;
    private final JMenuItem loadFileMenu;
    private final JMenuItem saveFileMenu;
    private final JMenuItem addAttributeMenu;
    private final JMenuItem startMenu;
    private final JMenuItem stopMenu;
    private final JMenuItem chooseFrequencyMenu;
    private final JMenuItem showTreeMenu;
    private final JMenuItem hideTreeMenu;
    private final JMenuItem showToolBarMenu;
    private final JMenuItem hideToolBarMenu;
    private final JMenuItem showChartManagementPanel;
    private final JMenuItem hideChartManagementPanel;
    private final JMenuItem showChartFreezePanel;
    private final JMenuItem hideChartFreezePanel;
    private final AttributeSelectionPanel attributePanel;
    private JDialog attributeSelectionDialog;

    private final DefaultMutableTreeNode node;
    private final SortTreeModel sortTreeModel;

    private final AbstractAction addAttributesAction;

    private final JToolBar toolBarPanel;
    private final JButton resetButton;
    private final JButton loadFileButton;
    private final JButton startButton;
    private final JButton stopButton;
    private final JButton chooseFrequencyButton;
    private final JButton saveFileButton;
    private final JButton addAttributeButton;
    private final JButton settingsButton;
    private String settingsDirectory;

    public TrendMenu(final CometeTrendController ctcontroller, final ChartPanel chartPanel, final CometeTrend parent) {
        super();

        String tangoHost;
        tangoHost = System.getProperty(TANGO_HOST);
        if (tangoHost == null) {
            tangoHost = System.getenv(TANGO_HOST);
        }
        node = new DefaultMutableTreeNode(tangoHost);

        sortTreeModel = new SortTreeModel(node);
        sortTreeModel.setComparator(new TreeNodeStringComparator());
        this.controller = ctcontroller;
        controller.addTrendFileListener(this);
        this.chartPanelReference = chartPanel == null ? null : new WeakReference<>(chartPanel);
        this.parentReference = parent == null ? null : new WeakReference<>(parent);

        // Init Component
        menuBar = new JMenuBar();
        menuFile = new JMenu();
        menuConfiguration = new JMenu();
        menuMonitoring = new JMenu();
        menuHelp = new JMenu();
        attributePanel = new AttributeSelectionPanel(controller);

        addAttributesAction = new AddAttributesAction();

        aboutMenu = new JMenuItem();
        loadFileMenu = new JMenuItem();
        saveFileMenu = new JMenuItem();
        addAttributeMenu = new JMenuItem(addAttributesAction);
        startMenu = new JMenuItem();
        stopMenu = new JMenuItem();
        chooseFrequencyMenu = new JMenuItem();
        showTreeMenu = new JMenuItem();
        hideTreeMenu = new JMenuItem();
        showToolBarMenu = new JMenuItem();
        hideToolBarMenu = new JMenuItem();
        showChartManagementPanel = new JMenuItem();
        hideChartManagementPanel = new JMenuItem();
        showChartFreezePanel = new JMenuItem();
        hideChartFreezePanel = new JMenuItem();

        toolBarPanel = new JToolBar(JToolBar.HORIZONTAL);
        resetButton = new JButton();
        settingsButton = new JButton();

        loadFileButton = new JButton();
        startButton = new JButton();
        stopButton = new JButton();
        TrendFrequencyAction trendFrequencyAction = new TrendFrequencyAction(ctcontroller);
        chooseFrequencyButton = new JButton(trendFrequencyAction);
        saveFileButton = new JButton();
        addAttributeButton = new JButton(addAttributesAction);

        // ToolBar
        toolBarPanel.addSeparator();
        toolBarPanel.add(addAttributeButton);
        toolBarPanel.addSeparator();
        toolBarPanel.add(loadFileButton);
        toolBarPanel.add(saveFileButton);
        toolBarPanel.addSeparator();
        toolBarPanel.add(startButton);
        toolBarPanel.add(stopButton);
        toolBarPanel.add(chooseFrequencyButton);
        toolBarPanel.add(resetButton);
        toolBarPanel.add(settingsButton);

        // Icon
        ImageIcon iconOpenFolder = ResourcesUtil.getIcon("CometeTrend.BoutonOpenFolderIcon");
        ImageIcon iconPlay = ResourcesUtil.getIcon("CometeTrend.BoutonPlayIcon");
        ImageIcon iconStop = ResourcesUtil.getIcon("CometeTrend.BoutonStopIcon");
        ImageIcon iconSave = ResourcesUtil.getIcon("CometeTrend.BoutonSaveIcon");
        ImageIcon iconRefresh = ResourcesUtil.getIcon("CometeTrend.Reset");
        ImageIcon iconSetting = ResourcesUtil.getIcon("CometeTrend.Setting.Chart");
        // Button add icons
        loadFileButton.setIcon(iconOpenFolder);
        saveFileButton.setIcon(iconSave);
        startButton.setIcon(iconPlay);
        stopButton.setIcon(iconStop);
        resetButton.setIcon(iconRefresh);
        settingsButton.setIcon(iconSetting);

        // Add tooltip to button
        loadFileButton.setToolTipText(ResourcesUtil.MESSAGES.getString("CometeTrend.ToolTipOpen"));
        startButton.setToolTipText(ResourcesUtil.MESSAGES.getString("CometeTrend.ToolTipPlay"));
        stopButton.setToolTipText(ResourcesUtil.MESSAGES.getString("CometeTrend.ToolTipStop"));
        saveFileButton.setToolTipText(ResourcesUtil.MESSAGES.getString("CometeTrend.ToolTipSave"));
        resetButton.setToolTipText(ResourcesUtil.MESSAGES.getString("CometeTrend.ToolTipReset"));
        settingsButton.setToolTipText(ResourcesUtil.MESSAGES.getString("CometeTrend.ToolTipSetting"));

        ActionListener playActionListener = (e) -> {
            playAction();
        };
        ActionListener settingsListener = (e) -> {
            settingsAction();
        };
        ActionListener stopActionListener = (e) -> {
            stopAction();
        };
        ActionListener saveFileActionListener = (e) -> {
            saveFileAction();
        };
        ActionListener loadActionListener = (e) -> {
            loadFileAction();
        };
        ActionListener resetActionListener = (e) -> {
            resetAction();
        };

        // Add action Listener for button
        loadFileButton.addActionListener(loadActionListener);
        saveFileButton.addActionListener(saveFileActionListener);
        startButton.addActionListener(playActionListener);
        stopButton.addActionListener(stopActionListener);
        settingsButton.addActionListener(settingsListener);
        resetButton.addActionListener(resetActionListener);

        // Action listener for show tree and hide tree
        showTreeMenu.addActionListener((e) -> {
            controller.setTreeVisible(true);
        });

        hideTreeMenu.addActionListener((e) -> {
            controller.setTreeVisible(false);
        });

        // Action listener for show toolbar and hide toolbar
        showToolBarMenu.addActionListener((e) -> {
            controller.setToolBarVisible(true);
        });

        hideToolBarMenu.addActionListener((e) -> {
            controller.setToolBarVisible(false);
        });

        // Action listeners for cahrt pnales
        showChartManagementPanel.addActionListener((e) -> {
            CometeTrend trend = ObjectUtils.recoverObject(parentReference);
            if (trend != null) {
                trend.setChartManagementPanelVisible(true);
            }
        });
        hideChartManagementPanel.addActionListener((e) -> {
            CometeTrend trend = ObjectUtils.recoverObject(parentReference);
            if (trend != null) {
                trend.setChartManagementPanelVisible(false);
            }
        });
        showChartFreezePanel.addActionListener((e) -> {
            CometeTrend trend = ObjectUtils.recoverObject(parentReference);
            if (trend != null) {
                trend.setChartFreezePanelVisible(true);
            }
        });
        hideChartFreezePanel.addActionListener((e) -> {
            CometeTrend trend = ObjectUtils.recoverObject(parentReference);
            if (trend != null) {
                trend.setChartFreezePanelVisible(false);
            }
        });

        aboutMenu.addActionListener((e) -> {
            if (helpDialog == null) {
                helpDialog = new AboutDialog(parentFrame);
            }
            helpDialog.setVisible(true);
            helpDialog.toFront();
        });

        // JMenuItem add icons and text

        saveFileMenu.setIcon((saveFileButton.getIcon()));
        loadFileMenu.setIcon((loadFileButton.getIcon()));
        startMenu.setIcon((startButton.getIcon()));
        stopMenu.setIcon((stopButton.getIcon()));
        chooseFrequencyMenu.setIcon((chooseFrequencyButton.getIcon()));

        saveFileMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemSave"));
        loadFileMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemLoad"));
        addAttributeMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemAddAtrribute"));
        startMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemPlay"));
        stopMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemStop"));
        chooseFrequencyMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemFrequency"));
        showTreeMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemShowTree"));
        hideTreeMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemHideTree"));
        showToolBarMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemShowToolBar"));
        hideToolBarMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemHideToolBar"));
        showChartManagementPanel
                .setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemShowChartManagementPanel"));
        hideChartManagementPanel
                .setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemHideChartManagementPanel"));
        showChartFreezePanel.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemShowChartFreezePanel"));
        hideChartFreezePanel.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemHideChartFreezePanel"));
        aboutMenu.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemAbout"));

        loadFileMenu.addActionListener(loadActionListener);
        saveFileMenu.addActionListener(saveFileActionListener);
        chooseFrequencyMenu.addActionListener(trendFrequencyAction);
        startMenu.addActionListener(playActionListener);
        stopMenu.addActionListener(stopActionListener);

        // Menu creation
        menuFile.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuFile"));
        menuFile.add(loadFileMenu);
        menuFile.addSeparator();
        menuFile.add(saveFileMenu);

        menuConfiguration.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuConfiguration"));
        menuConfiguration.add(addAttributeMenu);
        menuConfiguration.addSeparator();
        menuConfiguration.add(showTreeMenu);
        menuConfiguration.add(hideTreeMenu);
        menuConfiguration.addSeparator();
        menuConfiguration.add(showChartManagementPanel);
        menuConfiguration.add(hideChartManagementPanel);
        menuConfiguration.add(showChartFreezePanel);
        menuConfiguration.add(hideChartFreezePanel);
        menuConfiguration.addSeparator();
        menuConfiguration.add(showToolBarMenu);
        menuConfiguration.add(hideToolBarMenu);

        menuMonitoring.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuMonitoring"));
        menuMonitoring.add(startMenu);
        menuMonitoring.add(stopMenu);
        menuMonitoring.addSeparator();
        menuMonitoring.add(chooseFrequencyMenu);

        menuHelp.setText(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuHelp"));
        menuHelp.add(aboutMenu);

        menuBar.add(menuFile);
        menuBar.add(menuConfiguration);
        menuBar.add(menuMonitoring);
        menuBar.add(menuHelp);

        startButton.setEnabled(true);
        stopButton.setEnabled(false);

        saveFileButton.setVisible(true);
        addAttributeButton.setVisible(true);
        saveFileMenu.setVisible(true);
        addAttributeMenu.setVisible(true);
        resetButton.setVisible(true);
    }

    public void enableStartMenu(boolean enable) {
        stopMenu.setEnabled(enable);
        stopButton.setEnabled(enable);
        startMenu.setEnabled(!enable);
        startButton.setEnabled(!enable);
        chooseFrequencyButton.setEnabled(enable);
        chooseFrequencyMenu.setEnabled(enable);
    }

    public void enableStopMenu(boolean enable) {
        stopMenu.setEnabled(!enable);
        stopButton.setEnabled(!enable);
        startMenu.setEnabled(enable);
        startButton.setEnabled(enable);
        chooseFrequencyButton.setEnabled(!enable);
        chooseFrequencyMenu.setEnabled(!enable);
    }

    public void enableHideTreeMenu(boolean enable) {
        hideTreeMenu.setEnabled(enable);
    }

    public void enableShowTreeMenu(boolean enable) {
        showTreeMenu.setEnabled(enable);
    }

    public void enableHideToolBarMenu(boolean enable) {
        hideToolBarMenu.setEnabled(enable);
    }

    public void enableShowToolBarMenu(boolean enable) {
        showToolBarMenu.setEnabled(enable);
    }

    public void enableHideChartManagementPanel(boolean enable) {
        hideChartManagementPanel.setEnabled(enable);
    }

    public void enableShowChartManagementPanel(boolean enable) {
        showChartManagementPanel.setEnabled(enable);
    }

    public void updateManagementPanelItems(boolean managementPanelVisible) {
        enableShowChartManagementPanel(!managementPanelVisible);
        enableHideChartManagementPanel(managementPanelVisible);
    }

    public void enableHideChartFreezePanel(boolean enable) {
        hideChartFreezePanel.setEnabled(enable);
    }

    public void enableShowChartFreezePanel(boolean enable) {
        showChartFreezePanel.setEnabled(enable);
    }

    public void updateFreezePanelItems(boolean freezePanelVisible) {
        enableShowChartFreezePanel(!freezePanelVisible);
        enableHideChartFreezePanel(freezePanelVisible);
    }

    public boolean isToolBarMenuVisible() {
        return toolBarPanel.isVisible();
    }

    public void setToolBarMenuVisible(boolean visible) {
        toolBarPanel.setVisible(visible);
    }

    public AbstractAction getAddAttributesAction() {
        return addAttributesAction;
    }

    private void playAction() {
        controller.setRefreshingPeriod(controller.getRefreshingPeriod());
        controller.playAction();
        enableStartMenu(true);
    }

    private void stopAction() {
        controller.stopRefreshing();
        enableStopMenu(true);
    }

    private void resetAction() {
        controller.resetAction();
    }

    private void settingsAction() {
        ChartPanel chartPanel = ObjectUtils.recoverObject(chartPanelReference);
        if (chartPanel != null) {
            chartPanel.chartPropertiesAction();
        }
    }

    private JFileChooser getFileChooser() {
        return settingsDirectory == null || settingsDirectory.trim().isEmpty() ? new JFileChooser()
                : new JFileChooser(settingsDirectory);
    }

    public void loadFileAction() {
        JFileChooser chooser = getFileChooser();
        chooser.setDialogType(JFileChooser.OPEN_DIALOG);
        int showSaveDialog = chooser.showOpenDialog(ObjectUtils.recoverObject(parentReference));
        if (showSaveDialog == JFileChooser.APPROVE_OPTION) {
            File selectedFile = chooser.getSelectedFile();
            if (selectedFile != null) {
                settingsDirectory = selectedFile.getParentFile().getAbsolutePath();
                String filename = selectedFile.getAbsolutePath();
                controller.loadFileAction(filename);
            }
        }
    }

    public void addAttributesAction() {
        if (attributeSelectionDialog == null) {
            attributeSelectionDialog = new JDialog(
                    WindowSwingUtils.getWindowForComponent(ObjectUtils.recoverObject(parentReference)));
            attributeSelectionDialog.setContentPane(attributePanel);
            attributeSelectionDialog.setModal(true);
        }
        if (!attributeSelectionDialog.isVisible()) {
            attributeSelectionDialog.pack();
            attributeSelectionDialog.setLocationRelativeTo(attributeSelectionDialog.getOwner());
            attributeSelectionDialog.setVisible(true);
        }
    }

    public void saveFileAction() {
        CometeTrend parent = ObjectUtils.recoverObject(parentReference);
        JFileChooser chooser = getFileChooser();
        chooser.setDialogType(JFileChooser.SAVE_DIALOG);
        int showSaveDialog = chooser.showSaveDialog(parent);
        if (showSaveDialog == JFileChooser.APPROVE_OPTION) {
            File selectedFile = chooser.getSelectedFile();
            if (selectedFile != null) {
                String filename = selectedFile.getAbsolutePath();
                try {
                    if (parent != null) {
                        settingsDirectory = selectedFile.getParentFile().getAbsolutePath();
                        controller.saveFileAction(filename,
                                parent.appendCometeTrendConfiguration(null, selectedFile.getAbsolutePath()).toString(),
                                parent);
                    }
                } catch (Exception e) {
                    // TODO Display an error dialog like a JXErrorPane.
                    e.printStackTrace();
                }
            }
        }
    }

    public JMenuBar getMenuBar() {
        return menuBar;
    }

    public JToolBar getToolBarPanel() {
        return toolBarPanel;
    }

    @Override
    public void trendFileEvent(TrendEvent event) {
        if (event != null) {
            EventType type = event.getType();
            final TrendFile trendFile = event.getSource();
            if (trendFile != null) {
                switch (type) {
                    case LOAD:
                        saveFileButton.setEnabled(false);
                        String file = trendFile.getFileName();
                        if ((file != null) && !file.trim().isEmpty()) {
                            try {
                                settingsDirectory = new File(file).getParentFile().getAbsolutePath();
                            } catch (Exception e) {
                                // Nothing to do: settingsDirectory can't be updated
                            }
                        }
                        break;
                    case MODIFY:
                        saveFileButton.setEnabled(true);
                        break;
                    case SAVE:
                        saveFileButton.setEnabled(false);
                        break;
                    default:
                        break;
                }
            }
        }

    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Abstract action to open "add attributes" dialog
     * 
     * @author GIRARDOT
     */
    protected class AddAttributesAction extends AbstractAction {

        private static final long serialVersionUID = -1881344364274870801L;

        public AddAttributesAction() {
            super();
            putValue(SMALL_ICON, ResourcesUtil.getIcon("CometeTrend.BoutonAddAttribute"));
            putValue(SHORT_DESCRIPTION, ResourcesUtil.MESSAGES.getString("CometeTrend.ToolTipAddAttribute"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            addAttributesAction();
        }
    }

}

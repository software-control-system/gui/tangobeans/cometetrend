package fr.soleil.comete.bean.trend.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.ref.WeakReference;
import java.text.Collator;
import java.util.Collection;
import java.util.TreeSet;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import fr.soleil.comete.bean.trend.controller.CometeTrendController;
import fr.soleil.comete.bean.trend.controller.ITrendFileListener;
import fr.soleil.comete.bean.trend.controller.TrendEvent;
import fr.soleil.comete.bean.trend.controller.TrendEvent.EventType;
import fr.soleil.comete.bean.trend.model.AttributeName;
import fr.soleil.comete.bean.trend.model.TrendFile;
import fr.soleil.comete.bean.trend.model.TrendTreeModel;
import fr.soleil.comete.bean.trend.view.renderer.TrendTreeCellRenderer;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.tree.ExpandableTree;

public class TrendTreePanel extends JPanel implements ITrendFileListener {

    private static final long serialVersionUID = -2198155928897066736L;

    private final ExpandableTree tree;

    private final JMenuItem showOnY1;
    private final JMenuItem showOnY2;
    private final JMenuItem removeAttribute;

    private final JMenuItem openATKPanel;

    private final JMenuItem addAttribute;

    private final JPopupMenu attributePopupMenu;
    private final JPopupMenu rootPopupMenu;
    private final JPopupMenu devicePopupMenu;
    private final WeakReference<CometeTrendController> controllerReference;

    public TrendTreePanel(final CometeTrendController controller, final AbstractAction addAttributesAction) {
        super(new BorderLayout());
        this.controllerReference = new WeakReference<>(controller);
        controller.addTrendFileListener(this);

        // ------TangoTree------\\

        tree = new ExpandableTree(new TrendTreeModel());
        add(tree, BorderLayout.CENTER);
//        tree.setCellRenderer(new CometeTrendTreeCellRenderer(controller));
        tree.setCellRenderer(new TrendTreeCellRenderer(controller));
        tree.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseReleased(MouseEvent e) {
                onPopupTrigger(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    int pointedRow = tree.getRowForLocation(e.getX(), e.getY());
                    // if user clicked on a node
                    if (pointedRow != -1) {
                        if (!tree.isRowSelected(pointedRow)) {
                            tree.setSelectionRow(pointedRow);
                        }
                    }
                }
                onPopupTrigger(e);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    TreePath path = tree.getSelectionPath();
                    if (path != null) {
                        Object obj = path.getLastPathComponent();
                        if (obj instanceof DefaultMutableTreeNode) {
                            DefaultMutableTreeNode node = (DefaultMutableTreeNode) obj;
                            if (node.isRoot() && node.isLeaf()) {
                                addAttributesAction
                                        .actionPerformed(new ActionEvent(tree, ActionEvent.ACTION_PERFORMED, null));
                            }
                        }
                    }
                } else {
                    onPopupTrigger(e);
                }
            }
        });

        // ------Tree Popup menu------\\

        attributePopupMenu = new JPopupMenu();

        showOnY1 = new JMenuItem(ResourcesUtil.MESSAGES.getString("CometeTrend.Action.ShowOnY1"),
                ResourcesUtil.getIcon("CometeTrend.Node.Item.AxisY1"));

        showOnY1.addActionListener((e) -> {
            showOnY1();
        });

        showOnY2 = new JMenuItem(ResourcesUtil.MESSAGES.getString("CometeTrend.Action.ShowOnY2"),
                ResourcesUtil.getIcon("CometeTrend.Node.Item.AxisY2"));

        showOnY2.addActionListener((e) -> {
            showOnY2();
        });

        removeAttribute = new JMenuItem(ResourcesUtil.MESSAGES.getString("CometeTrend.Action.RemoveAttribute"),
                ResourcesUtil.getIcon("CometeTrend.Node.Item.AxisNone"));
        removeAttribute.addActionListener((e) -> {
            removeAtttributeAxis();
        });

        openATKPanel = new JMenuItem(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuItemOpenATKPanel"),
                ResourcesUtil.getIcon("CometeTrend.PopupMenu.Application"));

        devicePopupMenu = new JPopupMenu();
        devicePopupMenu.add(openATKPanel);

        attributePopupMenu.add(showOnY1);
        attributePopupMenu.add(showOnY2);

        attributePopupMenu.add(removeAttribute);

        rootPopupMenu = new JPopupMenu();

        addAttribute = new JMenuItem(addAttributesAction);
        String text = addAttribute.getText();
        if (text == null || text.isEmpty()) {
            addAttribute.setText(addAttribute.getToolTipText());
            addAttribute.setToolTipText(null);
        }

        rootPopupMenu.add(addAttribute);

        showOnY1.setVisible(true);
        showOnY2.setVisible(true);
        removeAttribute.setVisible(true);
        openATKPanel.setVisible(true);
        addAttribute.setVisible(true);

    }

    /*protected void addAttributes(Collection<String> attributes) {
        if (attributes != null) {
            AttributeSource source;
            ISourceDevice sourceDevice = attributeSelector.getSourceDevice();
            if (sourceDevice instanceof AttributeSource) {
                source = (AttributeSource) sourceDevice;
            } else {
                source = new AttributeSource();
                attributeSelector.setSourceDevice(source);
            }
            for (String attribute : attributes) {
                source.addAttribute(attribute);
            }
        }
    }*/

    // ------PopupTrigger------\\

    private void onPopupTrigger(final MouseEvent e) {
        if (e.isPopupTrigger()) {
            int pointedRow = tree.getRowForLocation(e.getX(), e.getY());

            if (pointedRow != -1) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
                if (node != null) {
                    if (node.isRoot()) {
                        rootPopupMenu.show(e.getComponent(), e.getX(), e.getY());
                    } else if (node.isLeaf()) {
                        attributePopupMenu.show(e.getComponent(), e.getX(), e.getY());
                    } else {
                        devicePopupMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        }
    }

    @Override
    public void trendFileEvent(TrendEvent event) {
        if (event != null && event.getSource() instanceof TrendFile) {
            EventType type = event.getType();
            TrendFile trendFile = event.getSource();
            if (trendFile != null) {
                switch (type) {
                    case MODIFY:
                        modifyFileAction(trendFile);
                        break;
                    case LOAD:
                        loadFileAction(trendFile);
                        break;
                    case ADD_ATTRIBUTE:
                        addAttributeAction(trendFile);
                        break;
                    case REMOVE_ATTRIBUTE:
                        removeAttributeAction(trendFile);
                        break;
                    case ATTRIBUTE_AXIS:
                        attributeAxisAction();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void loadFileAction(TrendFile trendFile) {
        reloadAttributes();
    }

    private void modifyFileAction(TrendFile trendFile) {

    }

    protected void reloadAttributes() {
        CometeTrendController controller = ObjectUtils.recoverObject(controllerReference);
        if (controller != null) {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                TrendTreeModel model = (TrendTreeModel) tree.getModel();
                model.reset();
                model.addAttibutes(controller.getAttributes());
                tree.expandAll(true);
            });
        }
        tree.repaint();
    }

    private void addAttributeAction(TrendFile trendFile) {
        reloadAttributes();
    }

    private void removeAttributeAction(TrendFile trendFile) {
        // TODO Action
    }

    private void attributeAxisAction() {
        // TODO Action
    }

    protected Collection<String> getSelectedAttributes() {
        Collection<String> selected = new TreeSet<>(Collator.getInstance());
        TreePath[] paths = tree.getSelectionPaths();
        if (paths != null) {
            for (TreePath path : paths) {
                Object comp = path.getLastPathComponent();
                if (comp instanceof DefaultMutableTreeNode) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) comp;
                    if (node.isLeaf() && (!node.isRoot())) {
                        Object obj = node.getUserObject();
                        if (obj instanceof AttributeName) {
                            selected.add(((AttributeName) obj).getFullName());
                        }
                    }
                }
            }
        }
        return selected;
    }

    protected void showOnY1() {
        CometeTrendController controller = ObjectUtils.recoverObject(controllerReference);
        if (controller != null) {
            Collection<String> selected = getSelectedAttributes();
            for (String attribute : selected) {
                controller.addToY1(attribute);
            }
        }
        tree.repaint();
    }

    protected void showOnY2() {
        CometeTrendController controller = ObjectUtils.recoverObject(controllerReference);
        if (controller != null) {
            Collection<String> selected = getSelectedAttributes();
            for (String attribute : selected) {
                controller.addToY2(attribute);
            }
        }
        tree.repaint();
    }

    protected void removeAtttributeAxis() {
        CometeTrendController controller = ObjectUtils.recoverObject(controllerReference);
        if (controller != null) {
            Collection<String> selected = getSelectedAttributes();
            for (String attribute : selected) {
                controller.removeAxis(attribute);
            }
        }
        tree.repaint();
    }

}

package fr.soleil.comete.bean.trend.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IKey;

public class AttributeSource implements ISourceDevice {

    public static final String PATH_SEPARATOR = ":";

    private List<String> attributeList;
    private String filename;
    private String description;

    public AttributeSource(final String filename, Set<IKey> keySet) {
        this.filename = filename;
        if ((keySet != null) && !keySet.isEmpty()) {
            attributeList = new ArrayList<>();
            IKey simpleKey = null;
            for (IKey key : keySet) {
                if (key instanceof HistoryKey) {
                    simpleKey = ((HistoryKey) key).getHistory();
                    String sourcePath = null;
                    if (simpleKey instanceof TangoKey) {
                        sourcePath = getTangoSourcePath((TangoKey) simpleKey);
                    }
                    if (sourcePath != null) {
                        attributeList.add(sourcePath);
                    }
                }
            }
            Collections.sort(attributeList);
        }
    }

    public AttributeSource() {
        attributeList = new ArrayList<>();
    }

    public boolean addAttribute(final String attribute) {
        boolean add = false;
        if ((attribute != null) && (!attribute.isEmpty()) && (!attributeList.contains(attribute))) {
            attributeList.add(attribute);
            add = true;
        }
        return add;
    }

    private String getTangoSourcePath(TangoKey key) {
        String attributeName = TangoKeyTool.getAttributeName(key);
        String deviceName = TangoKeyTool.getDeviceName(key);
        return deviceName + PATH_SEPARATOR + attributeName;
    }

    public List<String> getAttributeList() {
        return attributeList;
    }

    @Override
    public List<String> getSourceList() throws SourceDeviceException {
        if ((attributeList == null) || attributeList.isEmpty()) {
            throw new SourceDeviceException("No attribute found !");
        }
        return attributeList;
    }

    @Override
    public String getName() {
        return filename;
    }

    @Override
    public String getInformation() {
        return "Trend file source";
    }

    @Override
    public String getPathSeparator() {
        return PATH_SEPARATOR;
    }

    public String getDescription() {
        return description;
    }

}

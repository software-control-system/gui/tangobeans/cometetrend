package fr.soleil.comete.bean.trend.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.HierarchyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.bean.trend.controller.CometeTrendController;
import fr.soleil.comete.bean.trend.controller.ITrendFileListener;
import fr.soleil.comete.bean.trend.controller.TrendEvent;
import fr.soleil.comete.bean.trend.controller.TrendEvent.EventType;
import fr.soleil.comete.bean.trend.model.TrendFile;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.CometeConstants;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.swing.Splash;
import fr.soleil.lib.project.swing.SplitPaneUtils;

public class CometeTrend extends JFrame implements ITrendFileListener {

    private static final long serialVersionUID = -7988386089369128306L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CometeTrend.class);

    private static final String CONSOLE_SEPARATOR = "\n*********************************************************************\n";
    private static final String COMMAND_NOGUI = "-nogui";
    private static final String COMMAND_DATA = "-data";
    private static final String COMMAND_HELP = "-help";
    private static final String SAMPLING = "sampling";
    private static final String NO_SAMPLING = "nosampling";

    private static final String FRAME_TITLE = ResourcesUtil.MESSAGES.getString("CometeTrend.WindowTitle");
    private static final String CHART_LOADER_TITLE = ResourcesUtil.MESSAGES.getString("CometeTrend.WindowTitle.Loader");
    private static final String TREND_FILER_TITLE = ResourcesUtil.MESSAGES.getString("CometeTrend.WindowTitle.Filer");

    public static final ImageIcon APPLICATION_ICON = ResourcesUtil.getIcon("CometeTrend.FrameIcon");

    private static final double DEFAULT_DIVIDER_LOCATION = 0.2;

    private final CometeTrendController controller;

    private final String version;
    private final JPanel mainPanel;
    private TrendMenu trendMenu;
    private ChartPanel chartPanel;
    private TrendTreePanel attributeTree;
    private JScrollPane attributeTreeScrollPane;
    private JSplitPane splitPane;
    private JTextArea console;
    private JScrollPane consoleScrollPane;
    private double dividerLocation;
    // this listener is a hack to start with correct divider location
    private HierarchyListener listener;

    // ----- Splash ---- \\
    private static final Splash SPLASH;
    static {
        SPLASH = new Splash(ResourcesUtil.getIcon("CometeTrend.Splash"), Color.BLACK);
        SPLASH.setTitle(ResourcesUtil.MESSAGES.getString("CometeTrend.Splash.Application.Title.Default"));
        SPLASH.setCopyright(ResourcesUtil.MESSAGES.getString("CometeTrend.Splash.Application.Copyright"));
        SPLASH.setMessage(ResourcesUtil.MESSAGES.getString("CometeTrend.Splash.Application.Message"));
        SPLASH.progress(0);
        SPLASH.setAlwaysOnTop(true);
    }

    public CometeTrend(final CometeTrendController controller) {
        super();
        this.controller = controller;
        mainPanel = new JPanel(new BorderLayout());
        setContentPane(mainPanel);
        controller.addTrendFileListener(this);
        dividerLocation = DEFAULT_DIVIDER_LOCATION;
        setIconImage(APPLICATION_ICON.getImage());
        setSize(1000, 800);

        version = ResourcesUtil.getFileJarVersion(CometeTrend.class);
//        String frameTitle = FRAME_TITLE + CometeConstants.SPACE + (CometeTrendController.GROUPED_REFRESHING ? "(grouped)" : "(classic)");
        String frameTitle = FRAME_TITLE;
        if ((version != null) && !version.isEmpty()) {
            frameTitle = frameTitle + CometeConstants.SPACE + version;
        }
        setTitle(frameTitle);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        // this is a hack to start with correct divider location
        listener = (e) -> {
            if (isShowing()) {
                removeHierarchyListener(listener);
                listener = null;
                SwingUtilities.invokeLater(() -> {
                    // CONTROLGUI-378: splitPane is null in chartloader and trendfiler cases
                    if ((splitPane != null) && splitPane.getDividerLocation() != 0) {
                        splitPane.setDividerLocation(dividerLocation);
                    }
                });
            }
        };
        addHierarchyListener(listener);
    }

    // init ChartPanel
    private void initChartPanel(final CometeTrendController controller, final boolean samplingEnabled) {
        SPLASH.setMessage("Build chart");
        chartPanel = new ChartPanel(controller);
        // CONTROLGUI-380: sampling management
        chartPanel.setChartSamplingEnabled(samplingEnabled);
    }

    // init console mode
    private void initConsole() {
        String frameTitle = TREND_FILER_TITLE;
        if ((version != null) && !version.isEmpty()) {
            frameTitle = frameTitle + CometeConstants.SPACE + version;
        }
        setTitle(frameTitle);
        console = new JTextArea();
        console.setEditable(false);
        console.setText("TrendFiler");
        consoleScrollPane = new JScrollPane(console);
        consoleScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        consoleScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        mainPanel.add(consoleScrollPane, BorderLayout.CENTER);
    }

    private void initSplitPane() {
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, attributeTreeScrollPane, chartPanel);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(dividerLocation);
    }

    private void initAttributeTree() {
        attributeTree = new TrendTreePanel(controller, trendMenu.getAddAttributesAction());
        attributeTreeScrollPane = new JScrollPane(attributeTree);
    }

    private void initTrendMenu() {
        trendMenu = new TrendMenu(controller, chartPanel, this);
    }

    // CometeTrend default panel
    private void initAttributeTrend(boolean samplingEnabled) {
        initChartPanel(controller, samplingEnabled);
        // CONTROLGUI-380: redirect settings
        chartPanel.setAttributeTrendSettings(true);
        chartPanel.setupDisplayDuration();
        SPLASH.progress(10);
        SPLASH.setMessage("Build menu");
        initTrendMenu();
        trendMenu.enableShowTreeMenu(false);
        trendMenu.enableHideTreeMenu(true);
        trendMenu.enableShowChartManagementPanel(false);
        trendMenu.enableHideChartManagementPanel(true);
        trendMenu.enableShowChartFreezePanel(false);
        trendMenu.enableHideChartFreezePanel(true);
        SPLASH.progress(60);

        SPLASH.setMessage("Build tree");
        initAttributeTree();
        SPLASH.progress(80);

        SPLASH.setMessage("Build menu bar");
        setJMenuBar(trendMenu.getMenuBar());
        SPLASH.progress(90);

        // ------JSplitPane------\\
        initSplitPane();

        // MainPanel add ToolBar and SplitPane
        mainPanel.add(trendMenu.getToolBarPanel(), BorderLayout.NORTH);
        mainPanel.add(splitPane, BorderLayout.CENTER);
    }

    // ChartLoader panel
    private void initChartLoader(boolean samplingEnabled) {
        String frameTitle = CHART_LOADER_TITLE;
        if ((version != null) && !version.isEmpty()) {
            frameTitle = frameTitle + CometeConstants.SPACE + version;
        }
        setTitle(frameTitle);
        SPLASH.progress(70);
        initChartPanel(controller, samplingEnabled);
        // CONTROLGUI-380: don't redirect settings
        chartPanel.setAttributeTrendSettings(false);
        chartPanel.setChartManagementPanelVisible(false);
        chartPanel.setChartFreezePanelVisible(false);
        SPLASH.progress(80);
        mainPanel.add(chartPanel, BorderLayout.CENTER);
    }

    private void modifyFileAction(TrendFile trendFile) {
        trendFile.setWindowBounds(getBounds());
        int dividerLocation = splitPane.getDividerLocation();
        trendFile.setTreeVisible(dividerLocation != 0);
    }

    @Override
    public void trendFileEvent(TrendEvent event) {
        if (event != null && event.getSource() instanceof TrendFile) {
            EventType type = event.getType();
            TrendFile trendFile = event.getSource();
            if (trendFile != null) {
                switch (type) {
                    case MODIFY:
                        modifyFileAction(trendFile);
                        break;
                    case LOAD:
                        loadFileAction(trendFile);
                        break;
                    case SAVE:
                        saveFileAction(trendFile);
                        break;
                    case ADD_ATTRIBUTE:
                        addAttributeAction(trendFile);
                        break;
                    case REMOVE_ATTRIBUTE:
                        removeAttributeAction(trendFile);
                        break;
                    case ATTRIBUTE_AXIS:
                        attributeAxisAction();
                    case TREE_VISIBLE:
                        setTreeVisible(controller.isTreeVisible());
                        break;
                    case TOOLBAR_VISIBLE:
                        setToolBarVisible(controller.isToolBarVisible());
                        break;
                    case CHART_MANAGEMENT_PANEL:
                        setChartManagementPanelVisible(controller.isChartManagementPanelVisible());
                        break;
                    case CHART_FREEZE_PANEL:
                        setChartFreezePanelVisible(controller.isChartFreezePanelVisible());
                        break;
                    case TRACE:
                        traceText(event.getMessageToTrace());
                        break;
                    default:
                }
            }
        }
    }

    public StringBuilder appendCometeTrendConfiguration(StringBuilder strBuilder, String fileName) {
        StringBuilder builder = strBuilder == null ? new StringBuilder() : strBuilder;
        builder.append(TrendFile.TOOLBAR_VISIBLE_KEY).append(TrendFile.SEPARATOR)
                .append(trendMenu.isToolBarMenuVisible()).append(ObjectUtils.NEW_LINE);
        builder.append(TrendFile.TREE_VISIBLE_KEY).append(TrendFile.SEPARATOR).append(isTreeVisible())
                .append(ObjectUtils.NEW_LINE);
        builder.append(TrendFile.MANAGEMENT_PANEL_VISIBLE).append(TrendFile.SEPARATOR)
                .append(chartPanel.isChartManagementPanelVisible()).append(ObjectUtils.NEW_LINE);
        builder.append(TrendFile.FREEZE_PANEL_VISIBLE).append(TrendFile.SEPARATOR)
                .append(chartPanel.isChartFreezePanelVisible()).append(ObjectUtils.NEW_LINE);
        Rectangle bounds = getBounds();
        builder.append(TrendFile.WINDOW_POS_KEY).append(TrendFile.SEPARATOR).append(bounds.x).append(',')
                .append(bounds.y).append(ObjectUtils.NEW_LINE);
        builder.append(TrendFile.WINDOW_SIZE_KEY).append(TrendFile.SEPARATOR).append(bounds.width).append(',')
                .append(bounds.height).append(ObjectUtils.NEW_LINE);
        builder.append(TrendFile.REFRESH_TIME_KEY).append(TrendFile.SEPARATOR).append(controller.getRefreshingPeriod())
                .append(ObjectUtils.NEW_LINE);
        return chartPanel.appendChartConfiguration(builder, fileName);
    }

    public StringBuilder appendCometeTrendConfiguration(StringBuilder builder) {
        return appendCometeTrendConfiguration(builder, null);
    }

    private void loadFileAction(TrendFile trendFile) {
        if (trendFile.getWindowBounds() != null) {
            Rectangle windowBounds = trendFile.getWindowBounds();
            setBounds(windowBounds);

            boolean treeVisible = controller.isTreeVisible();
            setTreeVisible(treeVisible);

            boolean toolbarVisible = controller.isToolBarVisible();
            setToolBarVisible(toolbarVisible);

            boolean managementVisible = controller.isChartManagementPanelVisible();
            setChartManagementPanelVisible(managementVisible);

            boolean freezeVisible = controller.isChartFreezePanelVisible();
            setChartFreezePanelVisible(freezeVisible);

            trendMenu.enableStartMenu(true);
        }
    }

    private String getLocalHost() {
        String hostName = SystemUtils.getComputerFullName();
        if (hostName == null || hostName.trim().isEmpty()) {
            hostName = "localhost";
        }
        return hostName;
    }

    private void loadDataFile(String fileName) {
        chartPanel.loadDataFile(fileName);
        setTitle(new File(fileName).getName() + "@" + getLocalHost());
    }

    private void loadTrendFile(String configFileName) {
        new TrendFile(configFileName, controller, true);
    }

    protected CfFileReader generateChartConfigurationFileReader() {
        CfFileReader reader = new CfFileReader();
        reader.setWhiteSpaceAsSeparator(false);
        return reader;
    }

    protected void applyWindowConfiguration(CfFileReader reader) {
        List<String> param = reader.getParam(TrendFile.WINDOW_SIZE_KEY);
        if ((param != null) && (param.size() == 2)) {
            setSize(TrendFile.convertToInteger(param.get(0), getWidth()),
                    TrendFile.convertToInteger(param.get(1), getHeight()));
        }
        param = reader.getParam(TrendFile.WINDOW_POS_KEY);
        if (param != null) {
            if (param.size() == 2) {
                setLocation(TrendFile.convertToInteger(param.get(0), getX()),
                        TrendFile.convertToInteger(param.get(1), getY()));
            } else if (param.size() == 1) {
                int x = TrendFile.convertToInteger(param.get(0), 0);
                setLocation(x, x);
            }
        }
    }

    private void loadDataAndConfigurationFile(String firstFile, String secondFile) {
        CfFileReader reader = generateChartConfigurationFileReader();
        if (ChartUtils.isPotentialDataFile(firstFile)) {
            if (reader.readFile(secondFile)) {
                chartPanel.setConfigurationReader(reader);
                SwingUtilities.invokeLater(() -> {
                    chartPanel.setChartManagementPanelVisible(false);
                    chartPanel.setChartFreezePanelVisible(false);
                    applyWindowConfiguration(reader);
                });
            }
            SwingUtilities.invokeLater(() -> {
                loadDataFile(firstFile);
            });
        } else {
            if (reader.readFile(firstFile)) {
                chartPanel.setConfigurationReader(reader);
                SwingUtilities.invokeLater(() -> {
                    chartPanel.setChartManagementPanelVisible(false);
                    chartPanel.setChartFreezePanelVisible(false);
                    applyWindowConfiguration(reader);
                });
            }
            SwingUtilities.invokeLater(() -> {
                loadDataFile(secondFile);
            });
        }
        mainPanel.revalidate();
        mainPanel.repaint();
    }

    private void saveFileAction(TrendFile trendFile) {
        trendFile.setWindowBounds(getBounds());
    }

    private void addAttributeAction(TrendFile trendFile) {
        // not managed
    }

    private void removeAttributeAction(TrendFile trendFile) {
        // not managed
    }

    private void attributeAxisAction() {
        // not managed
    }

    private boolean isTreeVisible() {
        return (splitPane != null) && (splitPane.getParent() == mainPanel);
    }

    private void setTreeVisible(boolean visible) {
        // Change only if necessary
        if (visible != isTreeVisible()) {
            if (visible) {
                mainPanel.remove(chartPanel);
                if (attributeTree == null) {
                    initAttributeTree();
                }
                if (dividerLocation < 0 || dividerLocation > 1) {
                    dividerLocation = DEFAULT_DIVIDER_LOCATION;
                }
                initSplitPane();
                mainPanel.add(splitPane, BorderLayout.CENTER);
                SwingUtilities.invokeLater(() -> {
                    splitPane.setDividerLocation(dividerLocation);
                });
            } else {
                if (splitPane != null) {
                    double tmp = SplitPaneUtils.getDividerLocation(splitPane);
                    if (!Double.isNaN(tmp)) {
                        dividerLocation = tmp;
                    }
                    mainPanel.remove(splitPane);
                    splitPane.remove(chartPanel);
                    splitPane = null;
                }
                mainPanel.add(chartPanel, BorderLayout.CENTER);
            }
            trendMenu.enableShowTreeMenu(!visible);
            trendMenu.enableHideTreeMenu(visible);
        }
        mainPanel.revalidate();
        mainPanel.repaint();
    }

    private void setToolBarVisible(boolean visible) {
        trendMenu.setToolBarMenuVisible(visible);
        trendMenu.enableHideToolBarMenu(visible);
        trendMenu.enableShowToolBarMenu(!visible);
        repaint();
    }

    public void setChartManagementPanelVisible(boolean visible) {
        if (chartPanel != null) {
            chartPanel.setChartManagementPanelVisible(visible);
        }
        if (trendMenu != null) {
            trendMenu.updateManagementPanelItems(visible);
        }
    }

    public void setChartFreezePanelVisible(boolean visible) {
        if (chartPanel != null) {
            chartPanel.setChartFreezePanelVisible(visible);
        }
        if (trendMenu != null) {
            trendMenu.updateFreezePanelItems(visible);
        }
    }

    private void traceText(String text) {
        if (console == null) {
            LOGGER.trace(text);
        } else {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                console.setText(console.getText() + CONSOLE_SEPARATOR + text);
            });
        }

    }

    public static void main(final String[] args) {
        // Pre-analyze arguments to define use case
        int argCount = 0;
        boolean help = false;
        // CONTROLGUI-380: deactivate sampling by default
        boolean samplingEnabled = false;
        Collection<String> filteredArguments = new ArrayList<>();
        Boolean chartDetected = null;
        boolean trendFiler = false;
        if (args != null) {
            for (String arg : args) {
                if (arg != null) {
                    if (COMMAND_HELP.equalsIgnoreCase(arg)) {
                        help = true;
                        chartDetected = Boolean.FALSE;
                    } else if (SAMPLING.equalsIgnoreCase(arg)) {
                        // CONTROLGUI-380: treat "sampling" argument
                        samplingEnabled = true;
                    } else if (NO_SAMPLING.equalsIgnoreCase(arg)) {
                        // CONTROLGUI-380: treat "nosampling" argument
                        samplingEnabled = false;
                    } else {
                        filteredArguments.add(arg);
                        switch (argCount) {
                            case 0:
                                if (COMMAND_NOGUI.equals(arg)) {
                                    chartDetected = Boolean.FALSE;
                                    trendFiler = true;
                                } else if (COMMAND_DATA.equals(arg)) {
                                    chartDetected = Boolean.TRUE;
                                } else if (ChartUtils.isPotentialDataFile(arg)) {
                                    chartDetected = Boolean.TRUE;
                                }
                                break;
                            default:
                                if ((chartDetected == null) && (argCount < 2)) {
                                    if (ChartUtils.isPotentialDataFile(arg)) {
                                        chartDetected = Boolean.TRUE;
                                    }
                                }
                                break;
                        }
                        argCount++;
                    }
                }
            }
        }
        final boolean chartLoader = chartDetected == null ? false : chartDetected.booleanValue();
        final String[] arguments = filteredArguments.toArray(new String[filteredArguments.size()]);
        filteredArguments.clear();
        // Display help
        if (help) {
            SPLASH.setVisible(false);
            System.out.println(
                    "[config_file_path]\tLaunch attributeTrend with a specified trend config (could be an ATKTrend file) file see file samples in $LIVE_ROOT/conf/attributetrend folder");
            System.out.println(COMMAND_NOGUI + "\t\t\tGenerate a data file without graphical Display");
            System.out.println(COMMAND_DATA
                    + " [data_file_path]\tLoad a data file generated by nogui mode or a \"Save Data\" Menu in a chart");
            System.exit(0);
        } else {
            final CometeTrendController cometeTrendController = new CometeTrendController();
            // Use case is defined : set SPLASH title and create CometeTrend
            String msgKey;
            if (trendFiler) {
                msgKey = "CometeTrend.Splash.Application.Title.Filer";
            } else if (chartLoader) {
                msgKey = "CometeTrend.Splash.Application.Title.Loader";
            } else {
                msgKey = "CometeTrend.Splash.Application.Title";
            }
            SPLASH.setTitle(ResourcesUtil.MESSAGES.getString(msgKey));
            final CometeTrend cometeTrend = new CometeTrend(cometeTrendController);
            final boolean allowSampling = samplingEnabled;
            // Setup CometeTrend according to arguments
            if ((arguments == null) || (arguments.length == 0)) {
                SwingUtilities.invokeLater(() -> {
                    cometeTrend.initAttributeTrend(allowSampling);
                    SPLASH.progress(100);
                    SPLASH.setVisible(false);
                    cometeTrend.setVisible(true);
                    cometeTrend.toFront();
                });
            } else {
                final boolean trendFilerMode = trendFiler;
                // GUI initializations must be done in EDT
                SwingUtilities.invokeLater(() -> {
                    if (trendFilerMode) {
                        cometeTrend.initConsole();
                    } else if (chartLoader) {
                        cometeTrend.initChartLoader(allowSampling);
                    } else {
                        cometeTrend.initAttributeTrend(allowSampling);
                    }
                    SPLASH.progress(100);
                    SPLASH.setVisible(false);
                    cometeTrend.setVisible(true);
                    cometeTrend.toFront();
                });
                if (arguments.length == 1) {
                    if (chartLoader) {
                        String arg = arguments[0];
                        if (!COMMAND_DATA.equals(arg)) {
                            // Load data file must be done in EDT
                            SwingUtilities.invokeLater(() -> {
                                cometeTrend.loadDataFile(arg);
                            });
                        }
                    } else {
                        // Open application only with file in argument if it is an ATK Trend configuration file
                        SwingUtilities.invokeLater(() -> {
                            cometeTrendController.loadFileAction(arguments[0]);
                        });
                    }
                } else {
                    // With minimum 2 args
                    String firstArg = arguments[0];
                    String fileName = arguments[1];
                    // Invoke in EDT to ensure finishing previous initializations
                    SwingUtilities.invokeLater(() -> {
                        switch (firstArg) {
                            case COMMAND_DATA:
                                // Load data file must be done in EDT
                                cometeTrend.loadDataFile(fileName);
                                break;
                            case COMMAND_NOGUI:
                                // loadTrendFile can be done outside of EDT: do it in a separated thread
                                new Thread("Load trend file: " + fileName) {
                                    @Override
                                    public void run() {
                                        cometeTrend.loadTrendFile(fileName);
                                    }
                                }.start();
                                break;
                            default:
                                // loadDataAndConfigurationFile can be done outside of EDT: do it in a separated thread
                                new Thread("Load data and configuration files: " + firstArg + " & " + fileName) {
                                    @Override
                                    public void run() {
                                        cometeTrend.loadDataAndConfigurationFile(firstArg, fileName);
                                    }
                                }.start();
                                break;
                        }
                    });
                }
            }
        }
    }

}

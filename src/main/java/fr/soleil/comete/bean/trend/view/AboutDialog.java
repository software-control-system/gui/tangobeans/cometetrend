package fr.soleil.comete.bean.trend.view;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;

import fr.soleil.lib.project.file.FileUtils;
import fr.soleil.lib.project.swing.dialog.JDialogUtils;

public class AboutDialog extends JDialog implements ActionListener {

    private static final long serialVersionUID = -8590555239710504782L;

    private JEditorPane htmlPanel;
    private JButton cancelButton;

    public AboutDialog(final Frame owner) {
        super(owner);

        setModal(true);
        setTitle(ResourcesUtil.MESSAGES.getString("CometeTrend.MenuHelp"));
        // allow to close the dialog with Esc
        JDialogUtils.installEscapeCloseOperation(this);

        // call cancel action when closing the dialog
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                onCancel();
            }
        });

        initComponents();
        layoutComponents();

        // adjust to real size
        pack();
        // center the dialog
        setLocationRelativeTo(getOwner());
    }

    private void initComponents() {
        cancelButton = new JButton(ResourcesUtil.MESSAGES.getString("HelpPanel.CloseButton"));
        cancelButton.addActionListener(this);

        htmlPanel = new JEditorPane();
        htmlPanel.setEditorKit(new HTMLEditorKit());
        htmlPanel.setEditable(false);

        // let's mimic panel background color
        htmlPanel.setBackground(UIManager.getColor("Panel.background"));

        htmlPanel.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(final HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    // if uri is a file, get a temp version extracted from the
                    // jar and ask OS to open it,
                    // else ask OS to browse the link
                    if (Desktop.isDesktopSupported()) {
                        try {
                            URI uri = e.getURL().toURI();
                            String scheme = uri.getScheme();
                            String file = e.getURL().getFile();

                            if (scheme.equalsIgnoreCase("jar")) {
                                String[] pathInfo = file.split("!/");
                                String jarFileName = pathInfo[0].replaceFirst("file:", "");
                                String entryName = pathInfo[1];

                                String extension = FileUtils.getExtension(file);
                                String fileName = file.substring(file.lastIndexOf("/") + 1, file.lastIndexOf("."));
                                openFileInJar(jarFileName, entryName, fileName, extension);
                            } else if (scheme.equalsIgnoreCase("file")) {
                                Desktop.getDesktop().browse(uri);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });

        htmlPanel.setText(createHtmlString());
    }

    private String createHtmlString() {
        String manual = AboutDialog.class.getResource("CometeTrendUserManual.pdf").toString();

        StringBuilder sb = new StringBuilder();
        sb.append("<html>\n");
        sb.append("  <table width='100%'>\n");
        sb.append("    <tr>\n");
        sb.append("      <td width='100%'>\n");
        sb.append("        ").append(((Frame) getOwner()).getTitle()).append("\n");
        sb.append("        <br/>\n");
        sb.append("      </td>\n");
        sb.append("    </tr>\n");
        sb.append("  </table>\n");
        sb.append("  <br/>\n");
        sb.append("  <u>" + ResourcesUtil.MESSAGES.getString("HelpPanel.Developers") + "</u>:\n");
        sb.append("  <ul>\n");
        sb.append("    <li>Kentin RODRIGUES (SOLEIL)</li>\n");
        sb.append("  </ul>\n");
        sb.append("  <a href='").append(manual).append("'>")
                .append(ResourcesUtil.MESSAGES.getString("HelpPanel.Manual")).append("</a>\n");
        sb.append("</html>\n");

        return sb.toString();
    }

    private void layoutComponents() {
        JScrollPane scrollPane = new JScrollPane(htmlPanel);

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(cancelButton);

        JPanel titlePane = new JPanel();
        // JLabel icon = new JLabel(CometeTrend.APPLICATION_ICON);
        // titlePane.add(icon);
        JLabel titleLabel = new JLabel(ResourcesUtil.MESSAGES.getString("CometeTrend.WindowTitle"));
        titleLabel.setFont(new Font(Font.DIALOG, Font.BOLD, 20));
        titlePane.add(titleLabel);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 5));
        contentPane.add(titlePane, BorderLayout.NORTH);
        contentPane.add(scrollPane, BorderLayout.CENTER);
        contentPane.add(buttonPanel, BorderLayout.SOUTH);

        setContentPane(contentPane);
    }

    private static void openFileInJar(final String jarFileName, final String entryName, final String fileName,
            final String extension) {
        try (JarFile jarFile = new JarFile(jarFileName);) {
            JarEntry entry = jarFile.getJarEntry(entryName);
            InputStream is = jarFile.getInputStream(entry);

            // create a temp file named after the one from the jar, with the same extension
            File tempFile = File.createTempFile(fileName, "." + extension);
            FileUtils.duplicateFile(is, tempFile, true);
            tempFile.deleteOnExit();
            Desktop.getDesktop().open(tempFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void onCancel() {
        setVisible(false);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        Object source = e.getSource();
        if (source == cancelButton) {
            onCancel();
        }
    }

}

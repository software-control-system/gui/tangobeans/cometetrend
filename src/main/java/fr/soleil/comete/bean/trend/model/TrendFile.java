package fr.soleil.comete.bean.trend.model;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.tango.utils.DevFailedUtils;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.soleil.comete.bean.trend.controller.CometeTrendController;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeColorUtil;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoConstHelper;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.Format;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.date.IDateConstants;
import fr.soleil.tango.clientapi.TangoAttribute;

public class TrendFile {

    private static final String COMMENT = "!";
    private static final String GENERATED_FILE = "Generated file: ";
    private static final String LONG = "LONG";
    private static final String MAXIMUM_NUMBER_OF_LINES_TITLE = "Maximum number of lines in generated file: ";

    private static final String ERROR_WHILE_ACCESSING_FILE = "Error while accessing file %s: %s";
    private static final String UNRECOGNIZED_REFRESHING_INTERVAL = "Unrecognized Refreshing Interval ";
    private static final String SET_1000_MS_BY_DEFAULT = " set 1000 (ms) by default.";
    private static final String DATA_SEPARATOR_TITLE = "Data separator: ";
    private static final String DATE_FORMAT_TITLE = "Date Format: ";
    private static final String UNRECOGNIZED_DATE_FORMAT = "Unrecognized date Format ";
    private static final String SET_IN_MS_BY_DEFAULT = " set in ms by default.";
    private static final String WINDOW_BOUNDS_MISS_ARGUMENT_NEED_4_ARGUMENTS = "Window bounds miss argument need 4 arguments";
    private static final String REFRESHING_INTERVAL = "Refreshing Interval: ";
    private static final String TAB_REPLACEMENT = "TAB";
    private static final String TAB = "\t";
    private static final String CREATED = " created...";
    private static final String TIME = "Time";
    private static final String OPEN_PARENTHESIS = "(";
    private static final String CLOSE_PARENTHESIS = ")";
    private static final String ERROR_WITH = "Error with ";
    private static final String START_WRITING_VALUES_IN_FILE = "Start writing values in file: ";
    private static final String ERROR = "ERROR: ";
    private static final String ONE = "1";
    private static final String ZERO = "0";
    private static final String TITLE_SEPARATOR = ": ";
    private static final String ATTRIBUTE_DOES_NOT_EXIST = " attribute does not exist!";
    public static final String SINGLE_QUOT = "'";
    private static final String NULL = "null";
    private static final String COMMA = ",";

    private static final String DATA = "_data.txt";
    public static final int AXIS_NONE = -1;
    public static final char SEPARATOR = ':';
    private static final String EXTENSION_SEPARATOR = ".";
    private static final String GRAPH_TITLE_KEY = "graph_title";
    private static final String LABEL_VISIBLE_KEY = "label_visible";
    private static final String LABEL_PLACEMENT_KEY = "label_placement";
    private static final String LABEL_FONT_KEY = "label_font";
    private static final String CHART_BACKGROUND_KEY = "chart_background";
    private static final String TITLE_FONT_KEY = "title_font";
    private static final String DISPLAY_DURATION_KEY = "display_duration";
    private static final String PRECISION_KEY = "precision";
    public static final String TOOLBAR_VISIBLE_KEY = "toolbar_visible";
    public static final String TREE_VISIBLE_KEY = "tree_visible";
    public static final String DATE_VISIBLE_KEY = "date_visible";
    public static final String MANAGEMENT_PANEL_VISIBLE = "management_panel_visible";
    public static final String FREEZE_PANEL_VISIBLE = "freeze_panel_visible";
    public static final String WINDOW_POS_KEY = "window_pos";
    public static final String WINDOW_SIZE_KEY = "window_size";
    private static final String SHOW_DEVICE_NAME_KEY = "show_device_name";
    public static final String REFRESH_TIME_KEY = "refresh_time";
    private static final String VISIBLE_KEY = "visible";
    private static final String GRID_KEY = "grid";
    private static final String SUBGRID_KEY = "subgrid";
    private static final String GRID_STYLE_KEY = "grid_style";
    private static final String MIN_KEY = "min";
    private static final String MAX_KEY = "max";
    private static final String AUTOSCALE_KEY = "autoscale";
    private static final String SCALE_KEY = "scale";
    private static final String FORMAT_KEY = "format";
    private static final String TITLE_KEY = "title";
    private static final String COLOR_KEY = "color";
    private static final String DV_PREFIX = "dv";
    private static final String DV_NUMBER_KEY = DV_PREFIX + "_number";
    private static final String DV_NAME_KEY = "_name";
    private static final String DV_DISPLAY_NAME_KEY = "_displayname";
    public static final String DV_SELECTED_KEY = "_selected";
    private static final String DV_AXIS_KEY = "_axis";
    private static final String DV_LINECOLOR_KEY = "_linecolor";
    private static final String DV_LINEWIDTH_KEY = "_linewidth";
    private static final String DV_LINESTYLE_KEY = "_linestyle";
    private static final String DV_FILLCOLOR_KEY = "_fillcolor";
    private static final String DV_FILLMETHOD_KEY = "_fillmethod";
    private static final String DV_FILLSTYLE_KEY = "_fillstyle";
    private static final String DV_VIEWTYPE_KEY = "_viewtype";
    private static final String DV_BARWIDTH_KEY = "_barwidth";
    private static final String DV_MARKERCOLOR_KEY = "_markercolor";
    private static final String DV_MARKERSIZE_KEY = "_markersize";
    private static final String DV_MARKERSTYLE_KEY = "_markerstyle";
    private static final String DV_A0_KEY = "_A0";
    private static final String DV_A1_KEY = "_A1";
    private static final String DV_A2_KEY = "_A2";
    private static final String DV_lABELVISIBLE_KEY = "_labelvisible";
    private static final String DV_CLICKABLE_KEY = "_clickable";

    // Useful only for ATKTrend
    private static final String GRAPH_BACKGROUND_KEY = "graph_background";
    private static final String FIT_DISPLAY_DURATION_KEY = "fit_display_duration";

    public static final int DEFAULT_REFRESHING_PERIOD = 1000;
    public static final boolean DEFAULT_TOOLBAR_VISIBLE = true;
    public static final boolean DEFAULT_TREE_VISIBLE = true;
    public static final boolean DEFAULT_CHART_MANAGEMENT_PANEL_VISIBLE = true;
    public static final boolean DEFAULT_CHART_FREEZE_PANEL_VISIBLE = true;

    // CometeTrendFile configuration parameter
    private static final String GENERATED_FILE_NAME = "# GeneratedFileName";
    private static final String MAXIMUM_NUMBER_OF_LINES = "# MaximumNumberOfLines";
    private static final String REFRESHING_PERIOD = "# RefreshingPeriod";
    private static final String REFRESHING_PERDIOD = "# RefreshingPerdiod"; // miswriting that exists in old files
    private static final String DATE_FORMAT_TAG = "# DateFormat";
    private static final String DATA_SEPARATOR_TAG = "# DataSeparator";
    private static final String ATTRIBUTE_LIST = "# AttributeList";
    private static final String WINDOW_BOUNDS = "# WindowBounds";
    private static final String DEFAULT_SEPARATOR = TAB;

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern(IDateConstants.US_DATE_FORMAT);

    private static final Map<String, String> ATTRIBUTE_FORMAT_MAP = new HashMap<>();
    private static final Map<String, TangoAttribute> TANGO_ATTRIBUTE_MAP = new HashMap<>();

    private final Map<String, String> parameterMap;

    private final String fileName;

    private boolean toolBarVisible;
    private boolean treeVisible;
    private boolean dateVisible;
    private boolean chartManagementPanelVisible, chartFreezePanelVisible;
    private CometeColor graphBackground;
    private boolean fitDisplayDuration;
    private Rectangle windowBound;
    private int showDeviceName;

    private AttributeSource attributeSource;

    private final CometeTrendController controller;
    private String generatedFileName;
    private boolean cometeTrendFile;

    private String dataSeparator;

    private FileWriter fileWriter;
    private volatile boolean readingProcess;
    private DateTimeFormatter dateFormat;
    private String datePattern;
    private String[] buffer;
    private int bufferIndex;

    private int colorIndex;

    public TrendFile(String fileName) {
        this(fileName, new CometeTrendController(), false, true);
    }

    public TrendFile(String fileName, CometeTrendController controller) {
        this(fileName, controller, false, true);
    }

    public TrendFile(String fileName, CometeTrendController controller, boolean nogui) {
        this(fileName, controller, nogui, true);
    }

    public TrendFile(String fileName, CometeTrendController controller, boolean nogui, boolean load) {
        toolBarVisible = true;
        treeVisible = DEFAULT_TREE_VISIBLE;
        dateVisible = true;
        chartManagementPanelVisible = DEFAULT_CHART_FREEZE_PANEL_VISIBLE;
        chartFreezePanelVisible = DEFAULT_CHART_FREEZE_PANEL_VISIBLE;
        graphBackground = new CometeColor(180, 180, 180);
        fitDisplayDuration = true;
        windowBound = null;
        showDeviceName = 0;
        attributeSource = null;
        generatedFileName = ObjectUtils.EMPTY_STRING;
        cometeTrendFile = false;
        dataSeparator = DEFAULT_SEPARATOR;
        fileWriter = null;
        readingProcess = false;
        dateFormat = null;
        datePattern = null;
        bufferIndex = 0;
        colorIndex = 0;

        this.parameterMap = new HashMap<>();
        this.controller = controller;
        this.fileName = fileName;
        if (load && (fileName != null) && !fileName.isEmpty()) {
            loadDataFile();
            if (nogui) {
                createDataFile();
                startWriteValues();
            } else {
                ChartProperties chartProperties = controller.generateChartProperties();

                // read general properties
                retrieveGeneralProperties(chartProperties);

                // read ChartProperties
                retrieveChartProperties(chartProperties);

                // read Axis X, Y1, Y2
                retrieveAllAxisProperties(chartProperties);

                controller.setChartProperties(chartProperties);

                // read key and associated PlotProperties
                retrieveAllPlotProperties();
            }
        }
    }

    private void loadDataFile() {
        File file = new File(fileName);
        if (file.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
                attributeSource = new AttributeSource();
                int separatorIndex;
                String line;
                boolean firstLine = true;
                boolean generatedFile = false;
                boolean readAttributes = false;
                boolean readMaxLines = false;
                boolean readRefreshingPeriod = false;
                boolean readDataSeparator = false;
                boolean readDateFormat = false;
                boolean changeWindow_bounds = false;
                int refreshTimePeriod = controller.getRefreshingPeriod();
                Boolean trendFilerFile = null;
                while ((line = reader.readLine()) != null) {
                    line = line.trim();
                    if ((line.startsWith(COMMENT) || line.isEmpty() || line.equals(ObjectUtils.NEW_LINE))
                            && ((trendFilerFile == null) || trendFilerFile.booleanValue())) {
                        // Ignore comment start with ! and backspace
                        continue;
                    }
                    // Test first line
                    if (firstLine) {
                        firstLine = false;
                        if (line.startsWith(GENERATED_FILE_NAME)) {
                            cometeTrendFile = true;
                            generatedFile = true;
                            trendFilerFile = Boolean.TRUE;
                            continue;
                        } else {
                            cometeTrendFile = false;
                            trendFilerFile = Boolean.FALSE;
                            int lastIndexOf = fileName.lastIndexOf(EXTENSION_SEPARATOR);
                            if (lastIndexOf > -1) {
                                generatedFileName = fileName.substring(0, lastIndexOf) + DATA;
                            } else {
                                generatedFileName = fileName + DATA;
                            }
                            traceMessage(GENERATED_FILE + generatedFileName);
                        }
                    }
                    if (cometeTrendFile) {
                        if (generatedFile) {
                            generatedFileName = line;
                            traceMessage(GENERATED_FILE + generatedFileName);
                            generatedFile = false;
                        } else if (line.startsWith(MAXIMUM_NUMBER_OF_LINES)) {
                            readMaxLines = true;
                        } else if (line.startsWith(REFRESHING_PERIOD) || line.startsWith(REFRESHING_PERDIOD)) {
                            readRefreshingPeriod = true;
                        } else if (line.startsWith(ATTRIBUTE_LIST)) {
                            readAttributes = true;
                        } else if (line.startsWith(DATA_SEPARATOR_TAG)) {
                            readDataSeparator = true;
                        } else if (line.startsWith(DATE_FORMAT_TAG)) {
                            readDateFormat = true;
                        } else if (line.startsWith(WINDOW_BOUNDS)) {
                            changeWindow_bounds = true;
                        } else if (readMaxLines) {
                            int bufferSize = Integer.parseInt(line);
                            if (bufferSize > 1) {
                                // bufferSize must be at least 2 and buffer is of length bufferSize - 1
                                // because of the title line (buffer does not contain that line)
                                buffer = new String[bufferSize - 1];
                                traceMessage(MAXIMUM_NUMBER_OF_LINES_TITLE + bufferSize);
                            }
                            readMaxLines = false;
                        } else if (readRefreshingPeriod) {
                            try {
                                refreshTimePeriod = Integer.parseInt(line);
                            } catch (NumberFormatException nexc) {
                                traceMessage(UNRECOGNIZED_REFRESHING_INTERVAL + line + SET_1000_MS_BY_DEFAULT);
                            }
                            readRefreshingPeriod = false;
                        } else if (readDataSeparator) {
                            setDataSeparator(line);
                            traceMessage(DATA_SEPARATOR_TITLE + line);
                            readDataSeparator = false;
                        } else if (readDateFormat) {
                            if (LONG.equalsIgnoreCase(line)) {
                                dateFormat = null;
                            } else {
                                traceMessage(DATE_FORMAT_TITLE + line);
                                try {
                                    dateFormat = DateTimeFormatter.ofPattern(line);
                                    datePattern = line;
                                } catch (Exception e) {
                                    traceMessage(UNRECOGNIZED_DATE_FORMAT + line + SET_IN_MS_BY_DEFAULT);
                                }
                            }
                            readDateFormat = false;
                        } else if (readAttributes) {
                            controller.addAttribute(line);
                            attributeSource.addAttribute(line);
                        } else if (changeWindow_bounds) {
                            String[] split = line.split(COMMA);
                            changeWindow_bounds = false;
                            if (split.length == 4) {
                                String xString = split[0];
                                String yString = split[1];
                                String widthString = split[2];
                                String heightString = split[3];

                                int x = Integer.parseInt(xString);
                                int y = Integer.parseInt(yString);
                                int width = Integer.parseInt(widthString);
                                int height = Integer.parseInt(heightString);

                                setWindowBounds(new Rectangle(x, y, width, height));
                            } else {
                                traceMessage(WINDOW_BOUNDS_MISS_ARGUMENT_NEED_4_ARGUMENTS + line);
                            }
                        }
                    } else {
                        separatorIndex = line.indexOf(SEPARATOR);
                        if (separatorIndex > 0) {
                            String key = line.substring(0, separatorIndex);
                            String value = line.substring(separatorIndex + 1);
                            parameterMap.put(key, value);
                        }
                    }
                } // end while ((line = reader.readLine()) != null)
                controller.setRefreshingPeriod(refreshTimePeriod);
                traceMessage(
                        DateUtil.elapsedTimeToStringBuilder(new StringBuilder(REFRESHING_INTERVAL), refreshTimePeriod)
                                .toString());
            } catch (IOException e) {
                traceMessage(String.format(ERROR_WHILE_ACCESSING_FILE, fileName, e.getMessage()));
            }
        } // end if (file.exists())
    }

    private void setDataSeparator(String adataSeparator) {
        if (adataSeparator.equalsIgnoreCase(TAB_REPLACEMENT)) {
            dataSeparator = TAB;
        } else {
            dataSeparator = adataSeparator;
        }
    }

    private void traceMessage(String message) {
        controller.traceMessage(message);
    }

    public String getFileName() {
        return fileName;
    }

    private void writeFirstLineInDataFile() throws IOException {
        fileWriter.write(TIME);
        if (datePattern != null) {
            fileWriter.write(OPEN_PARENTHESIS + datePattern + CLOSE_PARENTHESIS);
        }
        List<String> sourceList = attributeSource.getAttributeList();
        for (String attribute : sourceList) {
            fileWriter.write(dataSeparator + attribute);
        }
    }

    private void shiftBuffer() {
        int length = buffer.length - 1;
        for (int i = 0; i < length; i++) {
            buffer[i] = buffer[i + 1];
        }
        if (length > -1) {
            buffer[length] = null;
        }
    }

    private void writeLineInDataFile(String line, boolean flush) throws IOException {
        fileWriter.write(ObjectUtils.NEW_LINE);
        fileWriter.write(line);
        if (flush) {
            fileWriter.flush();
        }
    }

    private void writeBufferInDataFile() throws IOException {
        for (String line : buffer) {
            writeLineInDataFile(line, false);
        }
        fileWriter.flush();
    }

    private void writeValuesInDataFile(List<String> values) {
        try {
            long time = System.currentTimeMillis();
            StringBuilder builder = new StringBuilder();
            if (dateFormat == null) {
                builder.append(time);
            } else {
                builder.append(DateUtil.milliToString(time, dateFormat));
            }
            for (String value : values) {
                builder.append(dataSeparator).append(value);
            }
            String line = builder.toString();
            builder.delete(0, builder.length());
            if ((buffer != null) && (buffer.length > 0)) {
                if (bufferIndex == buffer.length) {
                    shiftBuffer();
                    bufferIndex--;
                    buffer[bufferIndex++] = line;
                    fileWriter.close();
                    initFileWriter();
                    writeFirstLineInDataFile();
                    writeBufferInDataFile();
                } else {
                    buffer[bufferIndex++] = line;
                    writeLineInDataFile(line, true);
                }
            } else {
                writeLineInDataFile(line, true);
            }
        } catch (IOException e) {
            traceMessage(ERROR + DateUtil.milliToString(System.currentTimeMillis(), DATE_FORMAT) + TITLE_SEPARATOR
                    + e.getMessage());
        }
    }

    private void createDataFile() {
        try {
            initFileWriter();
            controller.traceMessage(generatedFileName + CREATED);
            writeFirstLineInDataFile();
            fileWriter.flush();
        } catch (IOException e2) {
            controller.traceMessage(ERROR_WITH + generatedFileName + e2.getMessage());
        }
    }

    private void initFileWriter() throws IOException {
        fileWriter = new FileWriter(generatedFileName);
    }

    private void startWriteValues() {
        readingProcess = true;
        controller.traceMessage(START_WRITING_VALUES_IN_FILE + generatedFileName);
        new TrendFilerThread().start();
    }

    private void readAttributesAndWriteDataFile() {
        List<String> attributeCompleteList = attributeSource.getAttributeList();
        if (attributeCompleteList != null && !attributeCompleteList.isEmpty()) {
            String deviceName = null;
            String attributeName = null;
            String attributeFullName = null;

            AttributeInfoEx attributeInfoEx = null;
            String format = null;
            TangoAttribute tangoAttribute = null;
            Object value = null;
            List<String> values = new ArrayList<>();

            for (String completeAttributeName : attributeCompleteList) {
                try {
                    attributeFullName = TangoUtil.getfullAttributeNameForAttribute(completeAttributeName);
                    deviceName = TangoUtil.getfullDeviceNameForAttribute(attributeFullName);
                    attributeName = TangoUtil.getAttributeName(attributeFullName);
                    if (TangoAttributeHelper.isAttributeRunning(deviceName, attributeName)) {
                        format = ATTRIBUTE_FORMAT_MAP.get(attributeFullName);
                        if (format == null) {
                            attributeInfoEx = TangoAttributeHelper.getAttributeInfoEx(deviceName, attributeName);
                            if (attributeInfoEx != null) {
                                format = attributeInfoEx.format;
                                ATTRIBUTE_FORMAT_MAP.put(attributeFullName, format);
                            }
                        }

                        tangoAttribute = TANGO_ATTRIBUTE_MAP.get(attributeFullName);
                        if (tangoAttribute == null) {
                            tangoAttribute = new TangoAttribute(attributeFullName);
                            TANGO_ATTRIBUTE_MAP.put(attributeFullName, tangoAttribute);
                        }

                        value = tangoAttribute.read();
                        String formattedValue;
                        if (value instanceof Boolean) {
                            formattedValue = ((Boolean) value).booleanValue() ? formattedValue = ONE : ZERO;
                        } else {
                            if (Format.isNumberFormatOK(format)) {
                                try {
                                    double doubleValue = (value instanceof Number ? ((Number) value).doubleValue()
                                            : Double.parseDouble(value.toString()));
                                    formattedValue = Format.formatValue(doubleValue, format);
                                } catch (Exception e) {
                                    formattedValue = String.valueOf(value);
                                }
                            } else {
                                formattedValue = String.valueOf(value);
                            }
                        }
                        values.add(String.valueOf(formattedValue).trim());
                    } else {
                        traceMessage(ERROR + DateUtil.milliToString(System.currentTimeMillis(), DATE_FORMAT)
                                + TITLE_SEPARATOR + attributeFullName + ATTRIBUTE_DOES_NOT_EXIST);
                        values.add(Double.toString(Double.NaN));
                    }
                } catch (DevFailed e) {
                    traceMessage(ERROR + DateUtil.milliToString(System.currentTimeMillis(), DATE_FORMAT)
                            + TITLE_SEPARATOR + DevFailedUtils.toString(e));
                    values.add(Double.toString(Double.NaN));
                } catch (Exception e) {
                    traceMessage(ERROR + DateUtil.milliToString(System.currentTimeMillis(), DATE_FORMAT)
                            + TITLE_SEPARATOR + e.getMessage());
                    values.add(Double.toString(Double.NaN));
                }
            }
            writeValuesInDataFile(values);
        }
    }

    private void retrieveGeneralProperties(ChartProperties chartProperties) {
        String toolbarVisible = parameterMap.get(TOOLBAR_VISIBLE_KEY);
        boolean toolBarConvert = convertToBoolean(toolbarVisible, toolBarVisible);
        setToolBarVisible(toolBarConvert);

        String treeVisibleValue = parameterMap.get(TREE_VISIBLE_KEY);
        boolean treeVisibleConvert = convertToBoolean(treeVisibleValue, treeVisible);
        setTreeVisible(treeVisibleConvert);

        String managementVisibleValue = parameterMap.get(MANAGEMENT_PANEL_VISIBLE);
        boolean managementConvert = convertToBoolean(managementVisibleValue, chartManagementPanelVisible);
        setChartManagementPanelVisible(managementConvert);

        String freezeVisibleValue = parameterMap.get(FREEZE_PANEL_VISIBLE);
        boolean freezeConvert = convertToBoolean(freezeVisibleValue, chartFreezePanelVisible);
        setChartFreezePanelVisible(freezeConvert);

        String dateVisibleValue = parameterMap.get(DATE_VISIBLE_KEY);
        boolean dateVisibleConvert = convertToBoolean(dateVisibleValue, dateVisible);
        setDateVisible(dateVisibleConvert);

        String fitDisplayDurationValue = parameterMap.get(FIT_DISPLAY_DURATION_KEY);
        boolean fitDisplayDurationConvert = convertToBoolean(fitDisplayDurationValue, fitDisplayDuration);
        setFitDisplayDuration(fitDisplayDurationConvert);

        String windowPos = parameterMap.get(WINDOW_POS_KEY);

        Point winLocation = convertToPoint(windowPos);
        String windowSize = parameterMap.get(WINDOW_SIZE_KEY);
        Point windowSizeConvert = convertToPoint(windowSize);
        if (windowSizeConvert.x == 0) {
            windowSizeConvert.x = 823;
        }

        if (windowSizeConvert.y == 0) {
            windowSizeConvert.y = 648;
        }

        setWindowBounds(new Rectangle(winLocation.x, winLocation.y, windowSizeConvert.x, windowSizeConvert.y));

        // chartProperties.set

        String showDeviceNameValue = parameterMap.get(SHOW_DEVICE_NAME_KEY);
        int showDeviceNameConvert = convertToInteger(showDeviceNameValue, showDeviceName);
        setShowDeviceName(showDeviceNameConvert);

        String refreshTime = parameterMap.get(REFRESH_TIME_KEY);
        int refreshTimeConvert = convertToInteger(refreshTime, controller.getRefreshingPeriod());
        controller.setRefreshingPeriod(refreshTimeConvert, false);
    }

//    @SuppressWarnings("unused")
//    private void updateChartProperties(PrintWriter writer) {
//        writer.println(GRAPH_TITLE_KEY + SEPARATOR + controller.getChartProperties().getTitle());
//    }

    private void retrieveChartProperties(ChartProperties chartProperties) {
        String title = parameterMap.get(GRAPH_TITLE_KEY);
        String titleConvert = convertToTitle(title);
        if (titleConvert != null) {
            chartProperties.setTitle(titleConvert);
        }

        String labelVisible = parameterMap.get(LABEL_VISIBLE_KEY);
        boolean labelVisibleBoolean = convertToBoolean(labelVisible, true);
        chartProperties.setLegendVisible(labelVisibleBoolean);

        String labelPlacement = parameterMap.get(LABEL_PLACEMENT_KEY);
        int labelPlacementConvert = convertToInteger(labelPlacement, 0);
        chartProperties.setLegendPlacement(labelPlacementConvert);

        String labelFont = parameterMap.get(LABEL_FONT_KEY);
        CometeFont labelFontConvert = convertToCometeFont(labelFont);
        chartProperties.setLabelFont(labelFontConvert);

        String chartBackGround = parameterMap.get(CHART_BACKGROUND_KEY);
        CometeColor chartBackGroundConvert = convertToCometeColor(chartBackGround,
                chartProperties.getBackgroundColor());
        chartProperties.setBackgroundColor(chartBackGroundConvert);

        String graphBackGround = parameterMap.get(GRAPH_BACKGROUND_KEY);
        CometeColor graphBackGroundConvert = convertToCometeColor(graphBackGround,
                chartProperties.getGlobalBackgroundColor());
        chartProperties.setGlobalBackgroundColor(graphBackGroundConvert);

        String displayDuration = parameterMap.get(DISPLAY_DURATION_KEY);
        double displayConvert = convertToDouble(displayDuration, 0);
        chartProperties.setDisplayDuration(displayConvert);

        String timePrecision = parameterMap.get(PRECISION_KEY);
        int convertTimePrecision = convertToInteger(timePrecision, 0);
        chartProperties.setTimePrecision(convertTimePrecision);

        String titleFont = parameterMap.get(TITLE_FONT_KEY);
        CometeFont titleFontConvert = convertToCometeFont(titleFont);
        chartProperties.setHeaderFont(titleFontConvert);

    }

    private void retrieveAllAxisProperties(ChartProperties chartProperties) {
        AxisPrefix[] keySet = AxisPrefix.values();
        for (AxisPrefix axisPrefix : keySet) {
            retrieveAxisProperties(axisPrefix, chartProperties);
        }
    }

    private void retrieveAxisProperties(AxisPrefix axisPrefix, ChartProperties chartProperties) {
        AxisProperties axisProperties = getAxisProperties(axisPrefix, chartProperties);

        if (axisProperties != null) {
            String axis = axisPrefix.toString();
            // get visible key
            String visibleValue = parameterMap.get(axis + VISIBLE_KEY);
            boolean axisVisible = convertToBoolean(visibleValue, true);
            axisProperties.setVisible(axisVisible);

            String gridKey = parameterMap.get(axis + GRID_KEY);
            boolean gridConvert = convertToBoolean(gridKey, false);
            axisProperties.setGridVisible(gridConvert);

            String subgridAxis = parameterMap.get(axis + SUBGRID_KEY);
            boolean subgridAxisConvert = convertToBoolean(subgridAxis, false);
            axisProperties.setSubGridVisible(subgridAxisConvert);

            String gridStyleAxis = parameterMap.get(axis + GRID_STYLE_KEY);
            int gridStyleConvertToInteger = convertToInteger(gridStyleAxis, 0);
            axisProperties.setGridStyle(gridStyleConvertToInteger);

            String minAxis = parameterMap.get(axis + MIN_KEY);
            double minConvert = convertToDouble(minAxis, 0);
            axisProperties.setScaleMin(minConvert);

            String maxAxis = parameterMap.get(axis + MAX_KEY);
            double maxConvert = convertToDouble(maxAxis, 0);
            axisProperties.setScaleMax(maxConvert);

            String autoscaleAxis = parameterMap.get(axis + AUTOSCALE_KEY);
            boolean autoScaleAxisConvert = convertToBoolean(autoscaleAxis, true);
            axisProperties.setAutoScale(autoScaleAxisConvert);

            String scale = parameterMap.get(axis + SCALE_KEY);
            int scaleConvert = convertToInteger(scale, 0);
            axisProperties.setScaleMode(scaleConvert);

            String labelFormat = parameterMap.get(axis + FORMAT_KEY);
            int labelFormatConvert = convertToInteger(labelFormat, 0);
            axisProperties.setLabelFormat(labelFormatConvert);

            String titleAxis = parameterMap.get(axis + TITLE_KEY);
            String titleAxisConvert = convertToTitle(titleAxis);
            if (titleAxis != null) {
                axisProperties.setTitle(titleAxisConvert);
            }
            String colorAxis = parameterMap.get(axis + COLOR_KEY);
            CometeColor colorAxisConvert = convertToCometeColor(colorAxis, axisProperties.getColor());
            axisProperties.setColor(colorAxisConvert);

            String labelFont = parameterMap.get(axis + LABEL_FONT_KEY);
            CometeFont labelFontConvert = convertToCometeFont(labelFont);
            axisProperties.setLabelFont(labelFontConvert);
        }
    }

    private void retrieveAllPlotProperties() {
        if (cometeTrendFile) {
            List<String> attributeList = attributeSource.getAttributeList();
            CometeColor cometeColor = CometeColor.RED;
            for (String fullAttributeName : attributeList) {
                String key = controller.parseKey(fullAttributeName);
                PlotProperties plotProperties = controller.getPlotProperties(key);
                if (plotProperties == null) {
                    plotProperties = new PlotProperties();
                    plotProperties.getCurve().setName(fullAttributeName);
                    if (colorIndex == CometeColorUtil.DEFAULT_COLORS.size() - 1) {
                        colorIndex = 0;
                    }
                    cometeColor = CometeColorUtil.DEFAULT_COLORS.get(colorIndex++);
                    plotProperties.getCurve().setColor(cometeColor);
                    IKey basicKey = ((HistoryKey) controller.convertToKey(fullAttributeName)).getHistory();
                    String attributeName = TangoKeyTool.getAttributeName(basicKey);
                    String deviceName = TangoKeyTool.getDeviceName(basicKey);
                    int tangoFormat = TangoAttributeHelper.getAttributeFormat(deviceName, attributeName);
                    if (tangoFormat == TangoConstHelper.BOOLEAN_FORMAT) {
                        plotProperties.setViewType(IChartViewer.TYPE_STAIRS);
                    }
                    controller.setPlotProperties(key, plotProperties);
                }
            }
        } else {
            // If it is an ATK file configuration
            String numberOfDv = parameterMap.get(DV_NUMBER_KEY);
            int number = convertToInteger(numberOfDv, 0);
            for (int index = 0; index < number; index++) {
                retrievePlotProperties(index);
            }
        }
    }

    private void retrievePlotProperties(int index) {
        String fullAttributeName = parameterMap.get(DV_PREFIX + index + DV_NAME_KEY);
        // Use display name to avoid case changes (CONTROLGUI-390)
        String expectedName = parameterMap.get(DV_PREFIX + index + DV_DISPLAY_NAME_KEY);
        if (expectedName != null && expectedName.indexOf('/') > -1) {
            fullAttributeName = expectedName;
        }
        if (fullAttributeName.indexOf('\'') == 0) {
            if (fullAttributeName.length() > 2) {
                fullAttributeName = fullAttributeName.substring(1, fullAttributeName.length() - 1);
            } else {
                fullAttributeName = ObjectUtils.EMPTY_STRING;
            }
        }
        String key = controller.parseKey(fullAttributeName);

        PlotProperties plotProperties = controller.getPlotProperties(key);
        if (plotProperties == null) {
            plotProperties = new PlotProperties();
            if (expectedName != null) {
                plotProperties.getCurve().setName(expectedName);
            }
            controller.setPlotProperties(key, plotProperties);
        }
        String clickableName = parameterMap.get(DV_PREFIX + index + DV_CLICKABLE_KEY);
        boolean clickableConvert = convertToBoolean(clickableName, true);
        controller.setClickable(key, clickableConvert);

        String labelVisible = parameterMap.get(DV_PREFIX + index + DV_lABELVISIBLE_KEY);
        boolean labelVisibleConvert = convertToBoolean(labelVisible, true);
        controller.setLabelVisible(key, labelVisibleConvert);

        // CurveProperties
        CurveProperties curveProperties = plotProperties.getCurve();
        curveProperties.setName(fullAttributeName);

        String curveColor = parameterMap.get(DV_PREFIX + index + DV_LINECOLOR_KEY);
        CometeColor curveColorConvert = convertToCometeColor(curveColor, curveProperties.getColor());
        curveProperties.setColor(curveColorConvert);

        String curveLineStyle = parameterMap.get(DV_PREFIX + index + DV_LINESTYLE_KEY);
        int curveLineConvert = convertToInteger(curveLineStyle, 0);
        curveProperties.setLineStyle(curveLineConvert);

        String curveLineWidth = parameterMap.get(DV_PREFIX + index + DV_LINEWIDTH_KEY);
        int curveLineWidthConvert = convertToInteger(curveLineWidth, 0);
        curveProperties.setWidth(curveLineWidthConvert);

        // MarkerProperties
        MarkerProperties markerProperties = plotProperties.getMarker();

        String markerColor = parameterMap.get(DV_PREFIX + index + DV_MARKERCOLOR_KEY);
        CometeColor markerColorConvert = convertToCometeColor(markerColor, markerProperties.getColor());
        markerProperties.setColor(markerColorConvert);

        String markerSize = parameterMap.get(DV_PREFIX + index + DV_MARKERSIZE_KEY);
        int markerSizeConvert = convertToInteger(markerSize, 0);
        markerProperties.setSize(markerSizeConvert);

        String markerStyle = parameterMap.get(DV_PREFIX + index + DV_MARKERSTYLE_KEY);
        int markerStyleConvert = convertToInteger(markerStyle, 0);
        markerProperties.setStyle(markerStyleConvert);

        // BarProperties
        BarProperties barProperties = plotProperties.getBar();

        String barColor = parameterMap.get(DV_PREFIX + index + DV_FILLCOLOR_KEY);
        CometeColor barColorConvert = convertToCometeColor(barColor, barProperties.getFillColor());
        barProperties.setFillColor(barColorConvert);

        String barMethod = parameterMap.get(DV_PREFIX + index + DV_FILLMETHOD_KEY);
        int barMethodrConvert = convertToInteger(barMethod, 0);
        barProperties.setFillingMethod(barMethodrConvert);

        String fillStyle = parameterMap.get(DV_PREFIX + index + DV_FILLSTYLE_KEY);
        int fillStyleConvert = convertToInteger(fillStyle, 0);
        barProperties.setFillStyle(fillStyleConvert);

        String barWidth = parameterMap.get(DV_PREFIX + index + DV_BARWIDTH_KEY);
        int barWidthConvert = convertToInteger(barWidth, 0);
        barProperties.setWidth(barWidthConvert);

        // TransformationProperties

        TransformationProperties transform = plotProperties.getTransform();

        String transform_A0 = parameterMap.get(DV_PREFIX + index + DV_A0_KEY);
        double transform_A0Convert = convertToDouble(transform_A0, 0);
        transform.setA0(transform_A0Convert);

        String transform_A1 = parameterMap.get(DV_PREFIX + index + DV_A1_KEY);
        double transform_A1Convert = convertToDouble(transform_A1, 0);
        transform.setA1(transform_A1Convert);

        String transform_A2 = parameterMap.get(DV_PREFIX + index + DV_A2_KEY);
        double transform_A2Convert = convertToDouble(transform_A2, 0);
        transform.setA2(transform_A2Convert);

        // PlotProperties
        String selected = parameterMap.get(DV_PREFIX + index + DV_SELECTED_KEY);
        int axisChoice;
        if (selected == null) {
            String axisStr = parameterMap.get(DV_PREFIX + index + DV_AXIS_KEY);
            if (axisStr == null) {
                axisChoice = AXIS_NONE;
            } else {
                try {
                    axisChoice = Integer.parseInt(axisStr);
                } catch (Exception e) {
                    axisChoice = AXIS_NONE;
                }
            }
        } else {
            axisChoice = convertToAxisChoice(selected);
        }
        plotProperties.setAxisChoice(axisChoice);

        String viewType = parameterMap.get(DV_PREFIX + index + DV_VIEWTYPE_KEY);
        int viewTypeConvert = convertToInteger(viewType, 0);
        plotProperties.setViewType(viewTypeConvert);

        IKey hKey = controller.convertToKey(fullAttributeName);

        if (hKey instanceof HistoryKey) {
            IKey basicKey = ((HistoryKey) hKey).getHistory();
            String attributeName = TangoKeyTool.getAttributeName(basicKey);
            String deviceName = TangoKeyTool.getDeviceName(basicKey);

            int tangoFormat = TangoAttributeHelper.getAttributeFormat(deviceName, attributeName);
            if (tangoFormat == TangoConstHelper.BOOLEAN_FORMAT) {
                plotProperties.setViewType(IChartViewer.TYPE_STAIRS);
            }
        }
    }

    public static String convertToTitle(String value) {
        String title = null;
        if ((value != null) && !value.trim().isEmpty()) {
            title = value.trim().replace(SINGLE_QUOT, ObjectUtils.EMPTY_STRING);
            if (title.equals(NULL)) {
                title = null;
            }
        }
        return title;
    }

    public static int convertToAxisChoice(String value) {
        int axisChoice = AXIS_NONE;
        if ((value != null) && !value.trim().isEmpty()) {
            int atkValue = convertToInteger(value, AXIS_NONE);
            switch (atkValue) {
                case 1:
                    axisChoice = IChartViewer.X;
                    break;
                case 2:
                    axisChoice = IChartViewer.Y1;
                    break;
                case 3:
                    axisChoice = IChartViewer.Y2;
                    break;
                default:
                    axisChoice = AXIS_NONE;
                    break;
            }
        }
        return axisChoice;
    }

    public static int convertToInteger(String value, int defaultValue) {
        int integer = defaultValue;
        if ((value != null) && !value.trim().isEmpty()) {
            try {
                integer = Integer.parseInt(value);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return integer;
    }

    public static double convertToDouble(String value, double defaultValue) {
        double number = defaultValue;
        if ((value != null) && !value.trim().isEmpty()) {
            try {
                number = Double.parseDouble(value);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return number;
    }

    public static boolean convertToBoolean(String value, boolean defaultBoolean) {
        boolean booleanValue = defaultBoolean;
        if ((value != null) && !value.trim().isEmpty()) {
            booleanValue = Boolean.parseBoolean(value);
        }

        return booleanValue;
    }

    public static CometeColor convertToCometeColor(String value, CometeColor defaultValue) {
        CometeColor cometeColor = defaultValue;
        if ((value != null) && !value.trim().isEmpty()) {
            String[] split = value.split(COMMA);
            if ((split != null) && (split.length == 3)) {
                int r = convertToInteger(split[0], 0);
                int g = convertToInteger(split[1], 0);
                int b = convertToInteger(split[2], 0);
                cometeColor = new CometeColor(r, g, b);
            }
        }
        return cometeColor;
    }

    public static Point convertToPoint(String value) {
        Point point = new Point(0, 0);
        if ((value != null) && !value.trim().isEmpty()) {
            String[] split = value.split(COMMA);
            if ((split != null) && (split.length == 2)) {
                int x = convertToInteger(split[0], 0);
                int y = convertToInteger(split[1], 0);
                point.x = x;
                point.y = y;
            }
        }
        return point;
    }

    public CometeFont convertToCometeFont(String value) {
        CometeFont cometeFont = null;
        if ((value != null) && !value.trim().isEmpty()) {
            String[] split = value.split(COMMA);
            if ((split != null) && (split.length == 3)) {
                String carac = split[0];
                int type = convertToInteger(split[1], 0);
                int size = convertToInteger(split[2], 0);
                cometeFont = new CometeFont(carac, type, size);
            }
        }
        return cometeFont;
    }

    public AxisProperties getAxisProperties(AxisPrefix axisPrefix, ChartProperties chartProperties) {
        AxisProperties axis = null;
        switch (axisPrefix) {
            case x:
                axis = chartProperties.getXAxisProperties();
                break;
            case y1:
                axis = chartProperties.getY1AxisProperties();
                break;
            case y2:
                axis = chartProperties.getY2AxisProperties();
                break;

            default:
                break;
        }
        return axis;
    }

    public void setAxisProperties(AxisPrefix axisPrefix, AxisProperties axisProperties) {
        if (axisProperties != null) {
            switch (axisPrefix) {
                case x:
                    controller.getChartProperties().setXAxisProperties(axisProperties);
                    break;
                case y1:
                    controller.getChartProperties().setY1AxisProperties(axisProperties);
                    break;
                case y2:
                    controller.getChartProperties().setY2AxisProperties(axisProperties);
                    break;

            }
        }
    }

    /**
     * This method is a wrapper to {@link CometeTrendController#getChartProperties()}
     * 
     * @return Some {@link ChartProperties}.
     */
    public ChartProperties getChartProperties() {
        return controller.getChartProperties();
    }

    /**
     * This method is a wrapper to {@link CometeTrendController#getKnownPlotProperties()}
     * 
     * @return The known {@link PlotProperties}.
     */
    public Collection<Entry<String, PlotProperties>> getKnownPlotProperties() {
        return controller.getKnownPlotProperties();
    }

    public boolean isToolBarVisible() {
        return toolBarVisible;
    }

    public void setToolBarVisible(boolean toolBarVisible) {
        this.toolBarVisible = toolBarVisible;
    }

    public boolean isTreeVisible() {
        return treeVisible;
    }

    public void setTreeVisible(boolean treeVisible) {
        this.treeVisible = treeVisible;
    }

    public boolean isDateVisible() {
        return dateVisible;
    }

    public void setDateVisible(boolean dateVisible) {
        this.dateVisible = dateVisible;
    }

    public boolean isChartManagementPanelVisible() {
        return chartManagementPanelVisible;
    }

    public void setChartManagementPanelVisible(boolean chartManagementPanelVisible) {
        this.chartManagementPanelVisible = chartManagementPanelVisible;
    }

    public boolean isChartFreezePanelVisible() {
        return chartFreezePanelVisible;
    }

    public void setChartFreezePanelVisible(boolean chartFreezePanelVisible) {
        this.chartFreezePanelVisible = chartFreezePanelVisible;
    }

    public int getShowDeviceName() {
        return showDeviceName;
    }

    public void setShowDeviceName(int showDeviceName) {
        this.showDeviceName = showDeviceName;
    }

    public boolean isFitDisplayDuration() {
        return fitDisplayDuration;
    }

    public void setFitDisplayDuration(boolean fitDisplayDuration) {
        this.fitDisplayDuration = fitDisplayDuration;
    }

    public Rectangle getWindowBounds() {
        return windowBound;
    }

    public void setWindowBounds(Rectangle rectangle) {
        windowBound = rectangle;
    }

    public CometeColor getGraphBackground() {
        return graphBackground;
    }

    public void setGraphBackground(CometeColor graphBackground) {
        this.graphBackground = graphBackground;
    }

    public AttributeSource getAttributeSource() {
        return attributeSource;
    }

    public IKey convertToKey(String value) {
        return controller.convertToKey(value);
    }

    public String parseKey(String value) {
        return controller.parseKey(value);
    }

    public static void main(String[] args) throws IOException {
        String fileName = "D:/TREND.txt";

        if ((args != null) && (args.length > 0)) {
            fileName = args[0];
        }
        CometeTrendController controller = new CometeTrendController();
        TrendFile trendFile = new TrendFile(fileName, controller);

        System.out.println("*****general Properties****");
        System.out.println(REFRESH_TIME_KEY + "=" + controller.getRefreshingPeriod());
        System.out.println(TOOLBAR_VISIBLE_KEY + "=" + trendFile.isToolBarVisible());
        System.out.println(TREE_VISIBLE_KEY + "=" + trendFile.isTreeVisible());
        System.out.println(SHOW_DEVICE_NAME_KEY + "=" + trendFile.getShowDeviceName());
        System.out.println(DATE_VISIBLE_KEY + "=" + trendFile.isDateVisible());
        System.out.println("windowBound=" + trendFile.getWindowBounds());
        System.out.println(GRAPH_BACKGROUND_KEY + "=" + trendFile.getGraphBackground());
        System.out.println(FIT_DISPLAY_DURATION_KEY + "=" + trendFile.isFitDisplayDuration());

        System.out.println("*****chartProperties****");
        ChartProperties chartProperties = controller.getChartProperties();

        System.out.println(GRAPH_TITLE_KEY + "=" + controller.getChartProperties().getTitle());
        System.out.println(CHART_BACKGROUND_KEY + "=" + controller.getChartProperties().getBackgroundColor());
        System.out.println(DISPLAY_DURATION_KEY + "=" + controller.getChartProperties().getDisplayDuration());
        System.out.println(TITLE_FONT_KEY + "=" + controller.getChartProperties().getHeaderFont());
        System.out.println(LABEL_FONT_KEY + "=" + controller.getChartProperties().getLabelFont());
        System.out.println(LABEL_PLACEMENT_KEY + "=" + controller.getChartProperties().getLegendPlacement());
        System.out.println(LABEL_VISIBLE_KEY + "=" + controller.getChartProperties().isLegendVisible());
        System.out.println(DISPLAY_DURATION_KEY + "=" + controller.getChartProperties().getDisplayDuration());
        System.out.println(PRECISION_KEY + "=" + controller.getChartProperties().getTimePrecision());

        System.out.println("*****axisProperties****");
        AxisPrefix[] values = AxisPrefix.values();
        for (AxisPrefix prefix : values) {
            System.out.println("------axis " + prefix + "------");
            AxisProperties axisProperties = trendFile.getAxisProperties(prefix, chartProperties);
            System.out.println(TITLE_KEY + "=" + axisProperties.getTitle());
            System.out.println(MAX_KEY + "=" + axisProperties.getScaleMax());
            System.out.println(MIN_KEY + "=" + axisProperties.getScaleMin());
            System.out.println(VISIBLE_KEY + "=" + axisProperties.isVisible());
            System.out.println(GRID_KEY + "=" + axisProperties.isGridVisible());
            System.out.println(SUBGRID_KEY + "=" + axisProperties.isSubGridVisible());
            System.out.println(GRID_STYLE_KEY + "=" + axisProperties.getGridStyle());
            System.out.println(AUTOSCALE_KEY + "=" + axisProperties.isAutoScale());
            System.out.println(SCALE_KEY + "=" + axisProperties.getScaleMode());
            System.out.println(FORMAT_KEY + "=" + axisProperties.getLabelFormat());
            System.out.println(COLOR_KEY + "=" + axisProperties.getColor());
            System.out.println(LABEL_FONT_KEY + "=" + axisProperties.getLabelFont());
        }

        System.out.println("*****key list Properties****");
        PlotProperties plotProperties = new PlotProperties();
        BarProperties bar = plotProperties.getBar();
        CurveProperties curve = plotProperties.getCurve();
        MarkerProperties marker = plotProperties.getMarker();
        for (Entry<String, PlotProperties> entry : controller.getKnownPlotProperties()) {
            String key = entry.getKey();
            System.out.println("key=" + key);
            System.out.println(DV_CLICKABLE_KEY + "=" + controller.isClickable(key));
            System.out.println(DV_lABELVISIBLE_KEY + "=" + controller.isLabelVisible(key));
            System.out.println(DV_VIEWTYPE_KEY + "=" + plotProperties.getViewType());

            // bar
            System.out.println(DV_FILLSTYLE_KEY + "=" + bar.getFillStyle());
            System.out.println(DV_FILLMETHOD_KEY + "=" + bar.getFillingMethod());
            System.out.println(DV_BARWIDTH_KEY + "=" + bar.getWidth());
            System.out.println(DV_FILLCOLOR_KEY + "=" + bar.getFillColor());
            // curve
            System.out.println(DV_LINESTYLE_KEY + "=" + curve.getLineStyle());
            System.out.println(DV_LINEWIDTH_KEY + "=" + curve.getWidth());
            System.out.println(DV_LINECOLOR_KEY + "=" + curve.getColor());
            // marker
            System.out.println(DV_MARKERSIZE_KEY + "=" + marker.getSize());
            System.out.println(DV_MARKERSIZE_KEY + "=" + marker.getColor());
            System.out.println(DV_MARKERSTYLE_KEY + "=" + marker.getStyle());

            plotProperties = entry.getValue();
            System.out.println("AxisChoice=" + plotProperties.getAxisChoice());
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    // ATKTrendFile configuration parameter
    public enum AxisPrefix {
        x, y1, y2;
    }

    /**
     * The thread that reads attributes and writes data file.
     * 
     * @author GIRARDOT
     */
    protected class TrendFilerThread extends Thread {

        public TrendFilerThread() {
            super("TrendFilerThread");
        }

        @Override
        public void run() {
            long delta = 0;
            while (readingProcess) {
                try {
                    long time = System.currentTimeMillis();
                    readAttributesAndWriteDataFile();
                    delta = System.currentTimeMillis() - time;
                    // try to compensate elapsed time
                    time = controller.getRefreshingPeriod() - delta;
                    if (time > 0) {
                        sleep(time);
                    }
                } catch (Exception e) {
                    controller.traceMessage(ERROR + e.getMessage());
                }
            }
        }
    }

}

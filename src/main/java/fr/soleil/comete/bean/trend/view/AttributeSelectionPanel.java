package fr.soleil.comete.bean.trend.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import fr.soleil.comete.bean.tangotree.TangoTree;
import fr.soleil.comete.bean.tangotree.data.AttributeFilter;
import fr.soleil.comete.bean.tangotree.data.DataDimension;
import fr.soleil.comete.bean.tangotree.data.DataLevel;
import fr.soleil.comete.bean.tangotree.data.DataType;
import fr.soleil.comete.bean.tangotree.model.TangoTreeModel;
import fr.soleil.comete.bean.tangotree.node.AttributeNode;
import fr.soleil.comete.bean.trend.controller.CometeTrendController;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class AttributeSelectionPanel extends JPanel {

    private static final long serialVersionUID = -8134128967134773486L;

    private final JButton addSelectedAttributesButton;
    private final JButton dismissButton;

    private final JScrollPane scrollpane;
    private final JPanel reloadPanel;
    private final JTree tangoTree;
    private final JButton reloadButton;
    private final JLabel reloadLabel;
    private final CometeTrendController controller;

    public AttributeSelectionPanel(CometeTrendController trendController) {
        super(new GridBagLayout());
        this.controller = trendController;
        addSelectedAttributesButton = new JButton("Add selected attribute(s)");
        addSelectedAttributesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (controller != null) {
                    controller.addAtttibutes(getSelectedAttributes());
                }
            }
        });
        reloadPanel = new JPanel(new BorderLayout(5, 5));
        reloadPanel.setOpaque(false);
        reloadPanel.setBorder(new EmptyBorder(0, 0, 5, 0));
        String reload = "Reload Tango Structure";
        reloadButton = new JButton(ResourcesUtil.getIcon("CometeTrend.Tango.Reload"));
        reloadButton.setToolTipText(reload);
        reloadButton.setMargin(CometeUtils.getzInset());
        reloadLabel = new JLabel(reload);
        reloadLabel.setFont(new Font(Font.DIALOG, Font.ITALIC, 11));
        reloadPanel.add(reloadButton, BorderLayout.WEST);
        reloadPanel.add(reloadLabel, BorderLayout.CENTER);

        dismissButton = new JButton("Dismiss");

        TangoTree tree = new TangoTree();
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        tree.setSelectableDataLevel(DataLevel.ATTRIBUTE);
        // TODO spectrum cases
        tree.setAttributeFilters(new AttributeFilter(DataDimension.SCALAR, DataType.NUMBER),
                new AttributeFilter(DataDimension.SCALAR, DataType.BOOLEAN));
//      tree.setAttributeFilters(new AttributeFilter(DataDimension.SCALAR, DataType.NUMBER),
//      new AttributeFilter(DataDimension.SPECTRUM, DataType.NUMBER));
        tangoTree = tree;

        scrollpane = new JScrollPane(tangoTree);
        scrollpane.setColumnHeaderView(reloadPanel);
        scrollpane.getColumnHeader().setBackground(Color.WHITE);
        scrollpane.setBackground(Color.WHITE);
        scrollpane.getHorizontalScrollBar().setBackground(Color.WHITE);
        scrollpane.getVerticalScrollBar().setBackground(Color.WHITE);
        GridBagConstraints scollPaneContrainst = new GridBagConstraints();
        scollPaneContrainst.fill = GridBagConstraints.BOTH;
        scollPaneContrainst.gridx = 0;
        scollPaneContrainst.gridy = 0;
        scollPaneContrainst.weightx = 1;
        scollPaneContrainst.weighty = 1;
        scollPaneContrainst.gridwidth = GridBagConstraints.REMAINDER;
        this.add(scrollpane, scollPaneContrainst);
        GridBagConstraints addSelContraints = new GridBagConstraints();
        addSelContraints.fill = GridBagConstraints.NONE;
        addSelContraints.gridx = 0;
        addSelContraints.gridy = 1;
        addSelContraints.weightx = 0;
        addSelContraints.weighty = 0;
        this.add(addSelectedAttributesButton, addSelContraints);
        GridBagConstraints dismissContraints = new GridBagConstraints();
        dismissContraints.fill = GridBagConstraints.NONE;
        dismissContraints.gridx = 2;
        dismissContraints.gridy = 1;
        dismissContraints.weightx = 0;
        dismissContraints.weighty = 0;
        this.add(dismissButton, dismissContraints);
        GridBagConstraints boxContrainst = new GridBagConstraints();
        boxContrainst.fill = GridBagConstraints.HORIZONTAL;
        boxContrainst.gridx = 1;
        boxContrainst.gridy = 1;
        boxContrainst.weightx = 1;
        boxContrainst.weighty = 0;
        this.add(Box.createGlue(), boxContrainst);

        reloadButton.addActionListener((e) -> {
            reloadTangoStructure();
        });

        dismissButton.addActionListener((e) -> {
            Window window = WindowSwingUtils.getWindowForComponent(dismissButton);
            if (window != null) {
                window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
                window.setVisible(false);
                window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSED));
            }
        });
//        reloadTangoStructure();
    }

    public JButton getAddSelectedAttributesButton() {
        return addSelectedAttributesButton;
    }

    protected void reloadTangoStructure() {
//        ISourceDevice tangoSource = new DatabaseTangoSourceDevice(true, true);
//        String pathSeparator = tangoSource.getPathSeparator();
//        try {
//            Collection<String> devices = tangoSource.getSourceList();
//            ITreeNode rootNode = new BasicTreeNode();
//
//            for (String path : devices) {
//                String[] pathSplit = path.split(pathSeparator);
//                ITreeNode currentNode = rootNode;
//                String nodePath = null;
//                String el = null;
//                for (String element : pathSplit) {
//                    el = element;
//                    ITreeNode node = null;
//                    for (ITreeNode child : currentNode.getChildren()) {
//                        if (child.getName().equals(el)) {
//                            node = child;
//                        }
//                    }
//                    if (node == null) {
//                        node = new BasicTreeNode();
//                    }
//                    node.setName(el);
//                    ITreeNode[] childToAdd = new ITreeNode[1];
//                    childToAdd[0] = node;
//                    currentNode.addNodes(childToAdd);
//
//                    if (nodePath == null) {
//                        nodePath = el;
//                    } else {
//                        nodePath = nodePath + pathSeparator + el;
//                    }
//                    node.setData(nodePath);
//                    currentNode = node;
//                }
//            }
//
//            rootNode.setName(tangoSource.getName());
//            rootNode.setToolTip(tangoSource.getInformation());
//            ((Tree) tangoTree).setRootNode(rootNode);
//        } catch (SourceDeviceException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        ((TangoTreeModel) tangoTree.getModel()).construct();
    }

    public Collection<String> getSelectedAttributes() {
        List<String> attributes = new ArrayList<>();
        TreePath[] paths = tangoTree.getSelectionPaths();
        if (paths != null) {
            for (TreePath path : paths) {
                if (path != null) {
                    Object comp = path.getLastPathComponent();
//                    if (comp instanceof DefaultMutableTreeNode) {
//                        DefaultMutableTreeNode node = (DefaultMutableTreeNode) comp;
//                        Object uo = node.getUserObject();
//                        if (uo instanceof ITreeNode) {
//                            ITreeNode treeNode = (ITreeNode) uo;
//                            if ((treeNode.getData() != null) && (node.getLevel() == 4)) {
//                                attributes.add(treeNode.getData().toString());
//                            }
//                        }
//                    }
                    if (comp instanceof AttributeNode) {
                        AttributeNode node = (AttributeNode) comp;
                        attributes.add(node.getFullPath());
                    }
                }
            }
        }
        Collections.sort(attributes, Collator.getInstance());
        return attributes;

    }

    /*  public List<String> getSelectedDevices() {
        List<String> selectedDevice = null;
        if (tangoTree != null) {
            ITreeNode[] nodes = tangoTree.getSelectedNodes();
            if (nodes != null) {
                selectedDevice = new ArrayList<String>();
                List<ITreeNode> children = null;
                for (ITreeNode node : nodes) {
                    children = node.getChildren();
                    if (((children == null) || children.isEmpty()) && node.getData() != null) {
                        selectedDevice.add(node.getData().toString());
                    }
                }
            }
        }
        return selectedDevice;
    }
    
    public void selectionChanged(final TreeNodeSelectionEvent event) {
        List<String> selectedDevices = getSelectedDevices();
        for (IDeviceSelectorListener listener : listeners) {
            listener.selectionChanged(selectedDevices);
    
        }
    
    }*/

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(AttributeSelectionPanel.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        AttributeSelectionPanel panel = new AttributeSelectionPanel(null);
        testFrame.setContentPane(panel);
        testFrame.setSize(400, 300);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}

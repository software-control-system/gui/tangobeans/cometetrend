package fr.soleil.comete.bean.trend.controller;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JOptionPane;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.bean.trend.controller.TrendEvent.EventType;
import fr.soleil.comete.bean.trend.model.TrendFile;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.GroupRefreshingStrategy;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.data.source.AbstractHistoryDataSource;
import fr.soleil.data.source.HistoryDataSourceProducer;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.ColorUtils;

public class CometeTrendController {

    public static final boolean GROUPED_REFRESHING;
    static {
        DataSourceProducerProvider.pushNewProducer(HistoryDataSourceProducer.class);
        String trueStr = "true";
        String prop = System.getProperty("GroupedRefreshing", trueStr);
        GROUPED_REFRESHING = trueStr.equalsIgnoreCase(prop);
    }

    public static final String REFESHING_GROUP = "AttributeTrend";

    private TrendFile trendFile;
    private final HistoryDataSourceProducer historyProducer;
    private final TrendFile defaultTrendFile;
    private final List<ITrendFileListener> trendFileListeners;
    private final List<String> attributeList;

    private final Map<String, PlotProperties> plotPropertiesMap;
    private final Map<String, Boolean> clickablePropertiesMap;
    private final Map<String, Boolean> labelVisiblePropertiesMap;
    private final Map<String, IKey> keyMap;
    private final Map<Integer, PolledRefreshingStrategy> refreshingMap;
    private final ColorUtils colorRotationTool;
    private ChartProperties chartProperties;
    private int refreshingPeriod;
    private long historyDepth;
    private boolean started;

    public CometeTrendController() {
        historyProducer = (HistoryDataSourceProducer) DataSourceProducerProvider
                .getProducer(HistoryDataSourceProducer.ID);
        plotPropertiesMap = new ConcurrentHashMap<>();
        clickablePropertiesMap = new ConcurrentHashMap<>();
        labelVisiblePropertiesMap = new ConcurrentHashMap<>();
        keyMap = new ConcurrentHashMap<>();
        refreshingMap = new ConcurrentHashMap<>();
        colorRotationTool = new ColorUtils();
        historyDepth = AbstractHistoryDataSource.DEFAULT_HISTORY_DEPTH;
        chartProperties = generateChartProperties();
        refreshingPeriod = TrendFile.DEFAULT_REFRESHING_PERIOD;
        attributeList = new ArrayList<>();
        defaultTrendFile = new TrendFile(ObjectUtils.EMPTY_STRING, this);
        trendFile = new TrendFile(ObjectUtils.EMPTY_STRING, this);
        trendFileListeners = new ArrayList<>();
    }

    public ChartProperties generateChartProperties() {
        ChartProperties chartProperties = new ChartProperties();
        chartProperties.setLegendPlacement(IChartViewer.LABEL_ROW);
        chartProperties.setAutoHighlightOnLegend(true);
        chartProperties.setLegendVisible(true);
        chartProperties.setDisplayDuration(getHistoryDepth(Double.NaN));
        return chartProperties;
    }

    public void addAtttibutes(String... attributes) {
        if (attributes != null) {
            for (String attribute : attributes) {
                addAttribute(attribute);
            }
        }
        fireTrendEvent(TrendEvent.EventType.ADD_ATTRIBUTE);
    }

    public void addAtttibutes(Collection<String> attributes) {
        if (attributes != null) {
            for (String attribute : attributes) {
                addAttribute(attribute);
            }
        }
        fireTrendEvent(TrendEvent.EventType.ADD_ATTRIBUTE);
    }

    // Load file
    public void loadFileAction(String fileName) {
        trendFile = new TrendFile(fileName, this);
        fireTrendEvent(EventType.LOAD);
    }

    // Tree visible

    public boolean isTreeVisible() {
        boolean treeVisible = TrendFile.DEFAULT_TREE_VISIBLE;
        if (trendFile != null) {
            treeVisible = trendFile.isTreeVisible();
        }
        return treeVisible;
    }

    public void setTreeVisible(boolean treeVisible) {
        if (trendFile != null) {
            trendFile.setTreeVisible(treeVisible);
        }
        fireTrendEvent(TrendEvent.EventType.TREE_VISIBLE);
    }

    public boolean isChartManagementPanelVisible() {
        boolean managementVisible = TrendFile.DEFAULT_CHART_MANAGEMENT_PANEL_VISIBLE;
        if (trendFile != null) {
            managementVisible = trendFile.isChartManagementPanelVisible();
        }
        return managementVisible;
    }

    public void setChartManagementPanelVisible(boolean chartManagementPanelVisible) {
        if (trendFile != null) {
            trendFile.setChartManagementPanelVisible(chartManagementPanelVisible);
        }
        fireTrendEvent(TrendEvent.EventType.CHART_MANAGEMENT_PANEL);
    }

    public boolean isChartFreezePanelVisible() {
        boolean freezeVisible = TrendFile.DEFAULT_CHART_FREEZE_PANEL_VISIBLE;
        if (trendFile != null) {
            freezeVisible = trendFile.isChartFreezePanelVisible();
        }
        return freezeVisible;
    }

    public void setChartFreezePanelVisible(boolean chartFreezePanelVisible) {
        if (trendFile != null) {
            trendFile.setChartFreezePanelVisible(chartFreezePanelVisible);
        }
        fireTrendEvent(TrendEvent.EventType.CHART_FREEZE_PANEL);
    }

    public void traceMessage(String message) {
        fireListenerEvent(message);
    }

    public void setToolBarVisible(boolean toolBarVisible) {
        if (trendFile != null) {
            trendFile.setToolBarVisible(toolBarVisible);
        }
        fireTrendEvent(TrendEvent.EventType.TOOLBAR_VISIBLE);
    }

    public boolean isToolBarVisible() {
        boolean toolBarVisible = TrendFile.DEFAULT_TOOLBAR_VISIBLE;
        if (trendFile != null) {
            toolBarVisible = trendFile.isToolBarVisible();
        }
        return toolBarVisible;
    }

    public TrendFile getTrendFile() {
        return trendFile;
    }

    protected void applyHistoryDepthNoCheck(IKey key) {
        long depth = historyProducer.getHistoryDepth(key);
        if (depth != historyDepth) {
            historyProducer.setHistoryDepth(key, historyDepth);
        }
    }

    public void applyHistoryDepth(IKey key) {
        if (historyProducer != null) {
            applyHistoryDepthNoCheck(key);
        }
    }

    protected void applyHistoryDepth() {
        if (historyProducer != null) {
            Collection<IKey> keys = keyMap.values();
            for (IKey key : keys) {
                applyHistoryDepthNoCheck(key);
            }
        }
    }

    protected void applyRefreshingPeriodNoCheck(IKey key, PolledRefreshingStrategy strategy) {
        historyProducer.setRefreshingStrategy(key, strategy);
    }

    protected PolledRefreshingStrategy getRefreshingStrategy() {
        Integer period = Integer.valueOf(refreshingPeriod);
        PolledRefreshingStrategy strategy = refreshingMap.get(period);
        if (strategy == null) {
            if (GROUPED_REFRESHING) {
                strategy = new GroupRefreshingStrategy(REFESHING_GROUP, period);
            } else {
                strategy = new PolledRefreshingStrategy(period);
            }
            refreshingMap.put(period, strategy);
        }
        return strategy;
    }

    protected void applyRefreshingPeriod() {
        if (historyProducer != null) {
            PolledRefreshingStrategy strategy = getRefreshingStrategy();
            historyProducer.setDefaultRefreshingStrategy(strategy);
            Collection<IKey> keys = keyMap.values();
            for (IKey key : keys) {
                applyRefreshingPeriodNoCheck(key, strategy);
                applyHistoryDepthNoCheck(key);
            }
            started = true;
        }
    }

    public void setRefreshingPeriod(int period, boolean autostart) {
        this.refreshingPeriod = period;
        if (autostart) {
            applyRefreshingPeriod();
        }
        fireTrendEvent(TrendEvent.EventType.REFRESH_FREQUENCY);
    }

    public void setRefreshingPeriod(int period) {
        setRefreshingPeriod(period, started);
    }

    public boolean isStarted() {
        return started;
    }

    public void setHistoryDepth(long historyDepth) {
        long depth = historyDepth > 0 ? historyDepth : AbstractHistoryDataSource.DEFAULT_HISTORY_DEPTH;
        if (this.historyDepth != depth) {
            this.historyDepth = depth;
            applyHistoryDepth();
        }
    }

    public void registerGroupListener(IRefreshingGroupListener groupListener) {
        if (historyProducer != null) {
            historyProducer.addRefreshingGroupListener(groupListener);
        }
    }

    public void unregisterGroupListener(IRefreshingGroupListener groupListener) {
        if (historyProducer != null) {
            historyProducer.removeRefreshingGroupListener(groupListener);
        }
    }

    public int getRefreshingPeriod() {
        return refreshingPeriod;
    }

    // Save file
    public void saveFileAction(String fileName, String chartConfiguration, Component parent) throws IOException {
        int ok = JOptionPane.YES_OPTION;
        File file = new File(fileName);
        if (file.exists()) {
            ok = JOptionPane.showConfirmDialog(parent, fileName + " exists. Overwrite?", "Confirm overwrite",
                    JOptionPane.YES_NO_OPTION);
        }
        if (ok == JOptionPane.YES_OPTION) {
            try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fileName),
                    StandardCharsets.UTF_8)) {
                writer.write(chartConfiguration);
            }
        }
        fireTrendEvent(TrendEvent.EventType.SAVE);
    }

    public void stopRefreshing() {
        started = false;
        if (historyProducer != null) {
            historyProducer.setDefaultRefreshingStrategy(null);
            Collection<IKey> keySet = keyMap.values();
            for (IKey key : keySet) {
                historyProducer.setRefreshingStrategy(key, null);
            }
        }
        fireTrendEvent(TrendEvent.EventType.STOP);
    }

    public void playAction() {
        applyRefreshingPeriod();
        fireTrendEvent(TrendEvent.EventType.PLAY);
    }

    public void resetAction() {
        if (historyProducer != null) {
            Collection<IKey> keySet = keyMap.values();
            for (IKey key : keySet) {
                historyProducer.resetHistory(key);
            }
        }
    }

    public void addTrendFileListener(final ITrendFileListener listener) {
        if ((listener != null) && (!trendFileListeners.contains(listener))) {
            trendFileListeners.add(listener);
        }
    }

    public void removeTrendFilListener(final ITrendFileListener listener) {
        if (listener != null && trendFileListeners.contains(listener)) {
            trendFileListeners.remove(listener);
        }
    }

    private void fireTrendEvent(TrendEvent.EventType type) {
        TrendFile source = defaultTrendFile;
        if (trendFile != null) {
            source = trendFile;
        }
        TrendEvent event = new TrendEvent(source, type);
        for (ITrendFileListener listener : trendFileListeners) {
            listener.trendFileEvent(event);
        }
    }

    private void fireListenerEvent(String message) {
        TrendFile source = defaultTrendFile;
        if (trendFile != null) {
            source = trendFile;
        }
        TrendEvent event = new TrendEvent(source, message);
        for (ITrendFileListener listener : trendFileListeners) {
            listener.trendFileEvent(event);
        }
    }

    public String parseKey(String value) {
        return value == null ? null : value.toLowerCase();
    }

    public IKey convertToKey(String value) {
        IKey key;
        if (value == null) {
            key = null;
        } else {
            key = keyMap.get(value);
            if (key == null) {
                IKey basicKey = null;
                if ((value != null) && !value.trim().isEmpty()) {
                    String completeAttributeName = value.trim().replace(TrendFile.SINGLE_QUOT,
                            ObjectUtils.EMPTY_STRING);
                    basicKey = createTangoKey(completeAttributeName);
                    if (!attributeList.contains(completeAttributeName)) {
                        attributeList.add(completeAttributeName);
                    }
                }
                if (basicKey != null) {
                    HistoryKey hkey = new HistoryKey(basicKey);
                    hkey.setUseSuffix(false);
                    key = hkey;
                }
                if (key != null) {
                    keyMap.put(value, key);
                }
            }
        }
        return key;
    }

    public IKey createTangoKey(String attributeName) {
        IKey tangoKey = new TangoKey();
        TangoKeyTool.registerAttribute(tangoKey, attributeName);
        TangoKeyTool.registerRefreshed(tangoKey, false);
        return tangoKey;
    }

    private void preparePlotProperties(String key, String attributeName) {
        if (key != null) {
            Color bg = ColorTool.getColor(chartProperties.getBackgroundColor());
            if (bg == null) {
                bg = Color.WHITE;
            }
            PlotProperties properties = plotPropertiesMap.get(key);
            if (properties == null) {
                properties = new PlotProperties(
                        ColorTool.getCometeColor(colorRotationTool.getNextColorFarEnoughFrom(bg)));
                properties.getCurve().setName(attributeName);
                properties.setAxisChoice(TrendFile.AXIS_NONE);
                plotPropertiesMap.put(key, properties);
            }
        }
    }

    public String[] getAttributes() {
        return attributeList.toArray(new String[attributeList.size()]);
    }

    public void addAttribute(String attributeFullName) {
        boolean addAttribute = attributeList.add(attributeFullName);
        if (addAttribute) {
            String key = parseKey(attributeFullName);
            preparePlotProperties(key, attributeFullName);
            traceMessage("Attribute: " + attributeFullName);
        }
    }

    public void addToY1(String attributeFullName) {
        String key = parseKey(attributeFullName);
        preparePlotProperties(key, attributeFullName);
        if (key != null) {
            plotPropertiesMap.get(key).setAxisChoice(IChartViewer.Y1);
            fireTrendEvent(TrendEvent.EventType.ATTRIBUTE_AXIS);
        }
    }

    public void addToY2(String attributeFullName) {
        String key = parseKey(attributeFullName);
        preparePlotProperties(key, attributeFullName);
        if (key != null) {
            plotPropertiesMap.get(key).setAxisChoice(IChartViewer.Y2);
            fireTrendEvent(TrendEvent.EventType.ATTRIBUTE_AXIS);
        }
    }

    public void removeAxis(String attributeFullName) {
        String key = parseKey(attributeFullName);
        preparePlotProperties(key, attributeFullName);
        if (key != null) {
            plotPropertiesMap.get(key).setAxisChoice(TrendFile.AXIS_NONE);
            fireTrendEvent(TrendEvent.EventType.ATTRIBUTE_AXIS);
        }
    }

    public Collection<Entry<String, PlotProperties>> getKnownPlotProperties() {
        return plotPropertiesMap.entrySet();
    }

    public PlotProperties getPlotProperties(String key) {
        return key == null ? null : plotPropertiesMap.get(parseKey(key));
    }

    public void setPlotProperties(String key, PlotProperties plotProperties) {
        if (key != null) {
            String id = parseKey(key);
            if (plotProperties == null) {
                plotPropertiesMap.remove(id);
            } else {
                plotPropertiesMap.put(id, plotProperties);
            }
        }
    }

    public ChartProperties getChartProperties() {
        return chartProperties;
    }

    public long getHistoryDepth(double displayDuration) {
        long depth;
        if (Double.isNaN(displayDuration) || displayDuration <= 0 || Double.isInfinite(displayDuration)) {
            depth = historyDepth;
        } else {
            depth = (long) displayDuration;
        }
        return depth;
    }

    public long getHistoryDepth(ChartProperties chartProperties) {
        long depth;
        if (chartProperties == null) {
            depth = historyDepth;
        } else {
            depth = getHistoryDepth(chartProperties.getDisplayDuration());
        }
        return depth;
    }

    public void setChartProperties(ChartProperties chartProperties) {
        this.chartProperties = chartProperties;
        setHistoryDepth(chartProperties == null ? historyDepth : getHistoryDepth(chartProperties));
        fireTrendEvent(EventType.CHART_PROPERTIES);
    }

    public boolean isClickable(String key) {
        boolean clickable;
        if (key == null) {
            clickable = false;
        } else {
            Boolean b = clickablePropertiesMap.get(parseKey(key));
            clickable = b == null ? false : b.booleanValue();
        }
        return clickable;
    }

    public void setClickable(String key, boolean cickable) {
        if (key != null) {
            clickablePropertiesMap.put(parseKey(key), Boolean.valueOf(cickable));
        }
    }

    public boolean isLabelVisible(String key) {
        Boolean visible = labelVisiblePropertiesMap.get(parseKey(key));
        return visible == null ? false : visible.booleanValue();
    }

    public void setLabelVisible(String key, boolean visible) {
        if (key != null) {
            labelVisiblePropertiesMap.put(parseKey(key), Boolean.valueOf(visible));
        }
    }

}

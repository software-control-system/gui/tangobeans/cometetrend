package fr.soleil.comete.bean.trend.controller;

import java.util.EventObject;

import fr.soleil.comete.bean.trend.model.TrendFile;

public class TrendEvent extends EventObject {

    private static final long serialVersionUID = 220780429065437407L;

    private final EventType type;
    private String messageToTrace;

    public TrendEvent(TrendFile source, EventType eventType) {
        super(source);
        this.type = eventType;
    }

    public TrendEvent(TrendFile source, String message) {
        super(source);
        this.type = EventType.TRACE;
        this.messageToTrace = message;
    }

    @Override
    public TrendFile getSource() {
        return (TrendFile) super.getSource();
    }

    public EventType getType() {
        return type;
    }

    public String getMessageToTrace() {
        return messageToTrace;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public enum EventType {
        LOAD, SAVE, ADD_ATTRIBUTE, REMOVE_ATTRIBUTE, STOP, PLAY, REFRESH_FREQUENCY, CONECT_ATTRIBUTE, ATTRIBUTE_AXIS, MODIFY, TREE_VISIBLE, TOOLBAR_VISIBLE, CHART_MANAGEMENT_PANEL, CHART_FREEZE_PANEL, CHART_PROPERTIES, PLOT_PROPERTIES, TRACE
    };

}

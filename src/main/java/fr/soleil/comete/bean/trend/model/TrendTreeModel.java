package fr.soleil.comete.bean.trend.model;

import java.text.Collator;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;

import fr.soleil.comete.bean.trend.comparator.TreeNodeStringComparator;
import fr.soleil.comete.bean.trend.view.ResourcesUtil;
import fr.soleil.lib.project.swing.model.SortTreeModel;

public class TrendTreeModel extends SortTreeModel {

    private static final long serialVersionUID = 1202131585639383209L;

    protected final Map<String, DefaultMutableTreeNode> nodeMap;

    public TrendTreeModel() {
        super(new DefaultMutableTreeNode(ResourcesUtil.ROOT_NODE));
        nodeMap = new TreeMap<>(Collator.getInstance());
        setComparator(new TreeNodeStringComparator());
    }

    public void reset() {
        deepRemoveNode(getRoot());
    }

    public void addAttibutes(String... attributes) {
        if (attributes != null) {
            DefaultMutableTreeNode root = getRoot();
            for (String attribute : attributes) {
                if (attribute != null) {
                    DefaultMutableTreeNode attributeNode = nodeMap.get(attribute);
                    if (attributeNode == null) {
                        int index = attribute.lastIndexOf('/');
                        if (index > -1) {
                            String device = attribute.substring(0, index);
                            DefaultMutableTreeNode deviceNode = nodeMap.get(device);
                            if (deviceNode == null) {
                                deviceNode = new DefaultMutableTreeNode(device);
                                nodeMap.put(device, deviceNode);
                                insertNodeInto(deviceNode, root, root.getChildCount());
                            }
                            AttributeName attributeName = new AttributeName(device, attribute.substring(index + 1),
                                    attribute);
                            attributeNode = new DefaultMutableTreeNode(attributeName);
                            nodeMap.put(attribute, attributeNode);
                            insertNodeInto(attributeNode, deviceNode, deviceNode.getChildCount());
                        }
                    }
                }
            }
        }
    }

    protected void deepRemoveNode(DefaultMutableTreeNode node) {
        if (node != null) {
            while (node.getChildCount() > 0) {
                DefaultMutableTreeNode child = (DefaultMutableTreeNode) node.getChildAt(0);
                deepRemoveNode(child);
            }
            if (!node.isRoot()) {
                Object obj = node.getUserObject();
                removeNodeFromParent(node);
                if (obj instanceof String) {
                    nodeMap.remove(obj);
                } else if (obj instanceof AttributeName) {
                    AttributeName attributeName = (AttributeName) obj;
                    nodeMap.remove(attributeName.getFullName());
                }
                node.setUserObject(null);
            }
        }
    }

}

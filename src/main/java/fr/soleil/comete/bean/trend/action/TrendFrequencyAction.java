package fr.soleil.comete.bean.trend.action;

import fr.soleil.comete.bean.trend.controller.CometeTrendController;
import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;

public class TrendFrequencyAction extends ChangeRefreshingPeriodAction {

    private static final long serialVersionUID = 9204158913223941312L;

    private CometeTrendController controller;

    public TrendFrequencyAction(CometeTrendController controller) {
        super();
        putValue(SHORT_DESCRIPTION, getValue(NAME));
        putValue(NAME, null);
        this.controller = controller;
    }

    @Override
    protected RefreshingPeriodPanel generateRefreshingPeriodPanel() {
        RefreshingPeriodPanel panel = new TrendPeriodPanel();
        panel.setPrecision(controller.getRefreshingPeriod());
        return panel;
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class TrendPeriodPanel extends RefreshingPeriodPanel {

        private static final long serialVersionUID = -1724919121126095099L;

        public TrendPeriodPanel() {
            super();
        }

        @Override
        public void apply() {
            if (computePrecision()) {
                controller.setRefreshingPeriod(getPrecision());
            }
        }
    }
}

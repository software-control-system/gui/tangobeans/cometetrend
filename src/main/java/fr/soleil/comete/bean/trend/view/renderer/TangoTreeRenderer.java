package fr.soleil.comete.bean.trend.view.renderer;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import fr.soleil.comete.bean.trend.view.ResourcesUtil;
import fr.soleil.comete.swing.util.CometeTreeCellRenderer;
import fr.soleil.comete.swing.util.ImageTool;

/**
 * @deprecated You should use {@link fr.soleil.comete.bean.tangotree.renderer.TangoTreeRenderer} instead.
 */
@Deprecated
public class TangoTreeRenderer extends CometeTreeCellRenderer {

    private static final long serialVersionUID = -7387302175770926431L;

    protected static final ImageIcon ROOT_ICON = ResourcesUtil.getIcon("CometeTrend.Tango");
    protected static final ImageIcon DOMAIN_ICON = ResourcesUtil.getIcon("CometeTrend.Domain");
    protected static final ImageIcon FAMILY_ICON = ResourcesUtil.getIcon("CometeTrend.Family");
    protected static final ImageIcon DEVICE_ICON = ResourcesUtil.getIcon("CometeTrend.Device");
    protected static final ImageIcon ATTRIBUTE_ICON = ImageTool
            .getImage(ResourcesUtil.getImage("CometeTrend.Attribute"));

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
            int row, boolean hasFocus) {
        JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        if (tree != null) {
            TreePath path = tree.getPathForRow(row);
            if (path != null) {
                Object tmp = path.getLastPathComponent();
                if (tmp instanceof DefaultMutableTreeNode) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) tmp;
                    switch (node.getLevel()) {
                        case 0:
                            label.setIcon(ROOT_ICON);
                            break;
//                        case 1:
//                            label.setIcon(DOMAIN_ICON);
//                            break;
//                        case 2:
//                            label.setIcon(FAMILY_ICON);
//                            break;
                        case 3:
                            label.setIcon(DEVICE_ICON);
                            break;
                        case 4:
                            label.setIcon(ATTRIBUTE_ICON);
                            break;
                    }
                }
            }
        }
        return label;
    }
}

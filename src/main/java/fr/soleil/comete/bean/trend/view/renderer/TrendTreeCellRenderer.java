package fr.soleil.comete.bean.trend.view.renderer;

import java.awt.Component;
import java.lang.ref.WeakReference;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import fr.soleil.comete.bean.trend.controller.CometeTrendController;
import fr.soleil.comete.bean.trend.model.AttributeName;
import fr.soleil.comete.bean.trend.model.TrendFile;
import fr.soleil.comete.bean.trend.view.ResourcesUtil;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.icons.DecorableIcon;

/**
 * {@link DefaultTreeCellRenderer} that can be used for trend tree.
 * 
 * @author GIRARDOT
 */
public class TrendTreeCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = -3939339747791922594L;

    private static final ImageIcon Y1_IMAGE = ResourcesUtil.getIcon("CometeTrend.Node.Item.AxisY1");
    private static final ImageIcon Y2_IMAGE = ResourcesUtil.getIcon("CometeTrend.Node.Item.AxisY2");
    private static final ImageIcon X_IMAGE = ResourcesUtil.getIcon("CometeTrend.Node.Item.AxisX");
    private static final ImageIcon NONE_IMAGE = ResourcesUtil.getIcon("CometeTrend.Node.Item.AxisNone");
    private static final ImageIcon ROOT_ICON = ResourcesUtil.getIcon("CometeTrend.Node.Root");
    private static final DecorableIcon ROOT_ICON_EXPANDED, ROOT_ICON_COLLAPSED;
    static {
        DecorableIcon icon = ResourcesUtil.getDecorableIcon("CometeTrend.Node.Root", "CometeTrend.Decoration.Collapse");
        icon.setDecorationHorizontalAlignment(SwingConstants.LEFT);
        icon.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        icon.setDecorated(true);
        ROOT_ICON_EXPANDED = icon;
        icon = ResourcesUtil.getDecorableIcon("CometeTrend.Node.Root", "CometeTrend.Decoration.Expand");
        icon.setDecorationHorizontalAlignment(SwingConstants.LEFT);
        icon.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        icon.setDecorated(true);
        ROOT_ICON_COLLAPSED = icon;
    }

    private final WeakReference<CometeTrendController> controllerReference;

    public TrendTreeCellRenderer(CometeTrendController controller) {
        super();
        this.controllerReference = controller == null ? null : new WeakReference<>(controller);
    }

    protected String getAttribute(Object value) {
        String attribute;
        if (value instanceof DefaultMutableTreeNode) {
            attribute = getAttribute(((DefaultMutableTreeNode) value).getUserObject());
        } else if (value instanceof AttributeName) {
            attribute = ((AttributeName) value).getFullName();
        } else {
            attribute = null;
        }
        return attribute;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf,
            int row, boolean hasFocus) {
        JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        if (row > 0) {
            if (leaf) {
                int axis = -1;
                CometeTrendController controller = ObjectUtils.recoverObject(controllerReference);
                if (controller != null) {
                    String key = controller.parseKey(getAttribute(value));
                    PlotProperties plotProperties = controller.getPlotProperties(key);
                    axis = plotProperties == null ? TrendFile.AXIS_NONE : plotProperties.getAxisChoice();
                }
                switch (axis) {
                    case IChartViewer.Y1:
                        label.setIcon(Y1_IMAGE);
                        break;
                    case IChartViewer.Y2:
                        label.setIcon(Y2_IMAGE);
                        break;
                    case IChartViewer.X:
                        label.setIcon(X_IMAGE);
                        break;
                    default:
                        label.setIcon(NONE_IMAGE);
                        break;
                }
            }
        } else if (value instanceof DefaultMutableTreeNode) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            if (node.isRoot()) {
                if (node.isLeaf()) {
                    label.setIcon(ROOT_ICON);
                } else if (tree.isExpanded(row)) {
                    label.setIcon(ROOT_ICON_EXPANDED);
                } else {
                    label.setIcon(ROOT_ICON_COLLAPSED);
                }
            }
        }
        return label;
    }

}

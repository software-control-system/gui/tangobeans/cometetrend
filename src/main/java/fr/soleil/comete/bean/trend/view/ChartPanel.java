package fr.soleil.comete.bean.trend.view;

import java.awt.BorderLayout;
import java.awt.Window;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import fr.soleil.comete.bean.trend.controller.CometeTrendController;
import fr.soleil.comete.bean.trend.controller.ITrendFileListener;
import fr.soleil.comete.bean.trend.controller.TrendEvent;
import fr.soleil.comete.bean.trend.controller.TrendEvent.EventType;
import fr.soleil.comete.bean.trend.model.TrendFile;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.data.target.complex.IDataArrayTarget;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.event.ChartViewerEvent.Reason;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.axis.model.AxisAttributes;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.util.ChartUtils;
import fr.soleil.comete.swing.util.CfFileReader;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.data.event.GroupEvent;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.swing.WindowSwingUtils;

public class ChartPanel extends JPanel implements ITrendFileListener, IChartViewerListener {

    private static final long serialVersionUID = -4323068809989070219L;

    private static final Object DEFAULT_DATA = new double[0];

    private final CometeTrendController controller;
    private final TrendChart chart;
    private final ChartViewerBox chartBox;
    private CfFileReader configurationReader;
    private boolean attributeTrendSettings;
    private final DataAccumulator dataAccumulator;
    private final AttributeGroupListener groupListener;

    public ChartPanel(CometeTrendController controller) {
        super(new BorderLayout());
        this.controller = controller;

        dataAccumulator = new DataAccumulator();
        groupListener = new AttributeGroupListener();

        chart = new TrendChart();
        chart.setAutoHighlightOnLegend(true);
        chartBox = new ChartViewerBox();
        if (CometeTrendController.GROUPED_REFRESHING) {
            chartBox.setSynchronTransmission(dataAccumulator, true);
        }

        // listeners (especially TrendFileListener) must be registered after Chart creation (see CONTROLGUI-390)
        controller.registerGroupListener(groupListener);
        controller.addTrendFileListener(this);

        add(chart, BorderLayout.CENTER);
    }

    public void setupDisplayDuration() {
        chart.removeChartViewerListener(this);
        chart.setDisplayDuration(controller.getHistoryDepth(Double.NaN));
        chart.addChartViewerListener(this);
    }

    @Override
    public void trendFileEvent(TrendEvent event) {
        if (event != null) {
            EventType type = event.getType();
            TrendFile trendFile = event.getSource();
            switch (type) {
                case MODIFY:
                    modifyFileAction(trendFile);
                    break;
                case LOAD:
                    loadFileAction(trendFile);
                    break;
                case ADD_ATTRIBUTE:
                    addAttributeAction(trendFile);
                    break;
                case REMOVE_ATTRIBUTE:
                    removeAttributeAction(trendFile);
                    break;
                case ATTRIBUTE_AXIS:
                    attributeAxisAction();
                    break;
                case CHART_PROPERTIES:
                    chart.removeChartViewerListener(this);
                    chart.setChartProperties(controller.getChartProperties());
                    chart.addChartViewerListener(this);
                    break;
                case PLOT_PROPERTIES:
                    // not managed as plot properties are apply during connectAttributes
                    break;
                default:
                    break;
            }
        }
    }

    private void updateChartSettingsDirectoryFromFile(String fileName) {
        if ((fileName != null) && !fileName.trim().isEmpty() && !ChartUtils.isPotentialDataFile(fileName)) {
            try {
                chart.setSettingsDirectory(new File(fileName).getParentFile().getAbsolutePath());
            } catch (Exception e) {
                // Nothing to do: settingsDirectory can't be updated
            }
        }
    }

    public StringBuilder appendChartConfiguration(StringBuilder builder) {
        return chart.appendConfiguration(builder);
    }

    public StringBuilder appendChartConfiguration(StringBuilder builder, String fileName) {
        updateChartSettingsDirectoryFromFile(fileName);
        return chart.appendConfiguration(builder);
    }

    @Override
    public void chartViewerChanged(ChartViewerEvent event) {
        if (event.getReason() == Reason.CONFIGURATION) {
            String id = event.getViewId();
            if (id == null) {
                controller.setChartProperties(chart.getChartProperties());
            } else {
                PlotProperties properties = chart.getDataViewPlotProperties(id);
                // TODO spectrum case
                int index = id.lastIndexOf(HistoryKey.SUFFIX);
                if (index > 0) {
                    id = id.substring(0, index);
                }
                controller.setPlotProperties(controller.parseKey(id), properties);
            }
        }
    }

    public boolean isAttributeTrendSettings() {
        return attributeTrendSettings;
    }

    public void setAttributeTrendSettings(boolean attributeTrendSettings) {
        this.attributeTrendSettings = attributeTrendSettings;
    }

    public void chartPropertiesAction() {
        chart.showChartOptionDialog();
    }

    private void modifyFileAction(TrendFile trendFile) {
        controller.setChartProperties(chart.getChartProperties());
    }

    public void loadDataFile(String filename) {
        chart.loadDataFile(filename);
    }

    public void applyConfiguration(CfFileReader f) {
        chart.applyConfiguration(f);
    }

    public boolean isChartManagementPanelVisible() {
        return chart.isManagementPanelVisible();
    }

    public void setChartManagementPanelVisible(boolean visible) {
        chart.setManagementPanelVisible(visible);
    }

    public boolean isChartFreezePanelVisible() {
        return chart.isFreezePanelVisible();
    }

    public void setChartFreezePanelVisible(boolean visible) {
        chart.setFreezePanelVisible(visible);
    }

    public void setChartSamplingEnabled(boolean enabled) {
        chart.setSamplingEnabled(enabled);
    }

    public CfFileReader getConfigurationReader() {
        return configurationReader;
    }

    public void setConfigurationReader(CfFileReader configurationReader) {
        this.configurationReader = configurationReader;
    }

    private void loadFileAction(TrendFile trendFile) {
        if (trendFile != null) {
            updateChartSettingsDirectoryFromFile(trendFile.getFileName());
        }
        controller.setRefreshingPeriod(controller.getRefreshingPeriod(), true);
        reconnectAttributes();
    }

    private void disconnectWidget(IKey key) {
        if (CometeTrendController.GROUPED_REFRESHING) {
            chartBox.disconnectWidget(dataAccumulator, key);
        } else {
            chartBox.disconnectWidget(chart, key);
        }
    }

    private void connectWidget(IKey key) {
        String id = key.getInformationKey();
        if (CometeTrendController.GROUPED_REFRESHING) {
            chartBox.connectWidget(dataAccumulator, key);
            // add curve after connection
            Map<String, Object> tmp = new HashMap<>();
            tmp.put(id, DEFAULT_DATA);
            if (!chart.hasData(id)) {
                chart.addData(tmp);
            }
            tmp.clear();
        } else {
            chartBox.connectWidget(chart, key);
        }
        if (controller != null) {
            controller.applyHistoryDepth(key);
        }
    }

    private void addAttributeAction(TrendFile trendFile) {
        connectAttributes();
    }

    protected void connectAttributes() {
        PlotProperties plotProperties = null;
        String dataViewId = null;
        chart.removeChartViewerListener(this);
        try {
            for (Entry<String, PlotProperties> entry : controller.getKnownPlotProperties()) {
                String key = entry.getKey();
                if (key != null) {
                    // TODO spectrum case
                    dataViewId = key;
                    plotProperties = entry.getValue();
                    int axisChoice = plotProperties.getAxisChoice();
                    chart.setDataViewPlotProperties(dataViewId, plotProperties);
                    chart.setDataViewAxis(dataViewId, axisChoice);
                    switch (axisChoice) {
                        case IChartViewer.X:
                        case IChartViewer.Y1:
                        case IChartViewer.Y2:
                            connectWidget(controller.convertToKey(key));
                            break;
                        default:
                            disconnectWidget(controller.convertToKey(key));
                            break;
                    }
                }
            }
            chart.setChartProperties(controller.getChartProperties());
        } finally {
            chart.addChartViewerListener(this);
        }
    }

    private void removeAttributeAction(TrendFile trendFile) {
        connectAttributes();
    }

    private void attributeAxisAction() {
        connectAttributes();
    }

    private void reconnectAttributes() {
        if (CometeTrendController.GROUPED_REFRESHING) {
            chartBox.disconnectWidgetFromAll(dataAccumulator);
            dataAccumulator.clearData();
        } else {
            chartBox.disconnectWidgetFromAll(chart);
        }
        chart.resetAll(true);
        connectAttributes();
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class TrendChart extends Chart {

        private static final long serialVersionUID = -8547324254385806788L;

        public TrendChart() {
            super();
        }

        public String getSettingsDirectory() {
            return settingsDirectory;
        }

        public void setSettingsDirectory(String directory) {
            if ((directory != null) && !directory.trim().isEmpty()) {
                this.settingsDirectory = directory;
            }
        }

        public boolean hasData(String id) {
            return (id != null) && data.containsKey(id);
        }

        public boolean isSamplingEnabled() {
            return getSamplingProperties().isEnabled();
        }

        public void setSamplingEnabled(boolean enabled) {
            SamplingProperties prop = getSamplingProperties();
            prop.setEnabled(false);
            setSamplingProperties(prop);
        }

        @Override
        protected DataView generateDataView(String id) {
            return new TrendDataView(id);
        }

        protected void doApplyConfiguration(CfFileReader f) {
            super.applyConfiguration(f);
        }

        @Override
        public void applyConfiguration(CfFileReader f) {
            if (isAttributeTrendSettings()) {
                // CONTROLGUI-380: load settings is linked to CometeTrend
                if (f != null) {
                    setAttributeTrendSettings(false);
                    controller.loadFileAction(f.getFilePath());
                    doApplyConfiguration(f);
                    setAttributeTrendSettings(true);
                }
            } else {
                doApplyConfiguration(f);
            }
        }

        @Override
        public String getConfiguration() {
            String configuration;
            if (isAttributeTrendSettings()) {
                // CONTROLGUI-380: save settings is linked to CometeTrend
                Window window = WindowSwingUtils.getWindowForComponent(this);
                if (window instanceof CometeTrend) {
                    configuration = ((CometeTrend) window).appendCometeTrendConfiguration(null).toString();
                } else {
                    configuration = super.getConfiguration();
                }
            } else {
                configuration = super.getConfiguration();
            }
            return configuration;
        }

        @Override
        protected void finalActionOnceDataLoaded() {
            // CONTROLGUI-379
            SwingUtilities.invokeLater(() -> {
                if (configurationReader != null) {
                    doApplyConfiguration(configurationReader);
                    configurationReader = null;
                }
            });
            super.finalActionOnceDataLoaded();
        }

        protected void transmitDisplayDuration(double displayDuration) {
            boolean fitDuration = (displayDuration > 0) && (!Double.isNaN(displayDuration))
                    && (!Double.isInfinite(displayDuration));
            AxisAttributes attributes = chartView.getAxis(X).getAttributes();
            if (attributes.isFitXAxisToDisplayDuration() != fitDuration) {
                attributes.setFitXAxisToDisplayDuration(fitDuration);
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    doRefresh(true, null);
                });
            }
            if (controller != null) {
                // CONTROLGUI-382
                controller.setHistoryDepth(controller.getHistoryDepth(displayDuration));
            }
        }

        @Override
        public void setDisplayDuration(double duration) {
            super.setDisplayDuration(duration);
            transmitDisplayDuration(duration);
        }

        @Override
        public void setChartProperties(ChartProperties properties) {
            super.setChartProperties(properties);
            if (properties != null) {
                transmitDisplayDuration(properties.getDisplayDuration());
            }
        }

    }

    protected static class TrendDataView extends DataView {

        public TrendDataView() {
            super();
        }

        public TrendDataView(String id) {
            super(id);
        }

        @Override
        public StringBuilder appendConfiguration(StringBuilder configurationBuilder, String prefix) {
            StringBuilder builder = super.appendConfiguration(configurationBuilder, prefix);
            int axis = getAxis();
            switch (axis) {
                case IChartViewer.X:
                    builder.append(prefix).append(TrendFile.DV_SELECTED_KEY).append(':').append(1)
                            .append(ObjectUtils.NEW_LINE);
                    break;
                case IChartViewer.Y1:
                    builder.append(prefix).append(TrendFile.DV_SELECTED_KEY).append(':').append(2)
                            .append(ObjectUtils.NEW_LINE);
                    break;
                case IChartViewer.Y2:
                    builder.append(prefix).append(TrendFile.DV_SELECTED_KEY).append(':').append(3)
                            .append(ObjectUtils.NEW_LINE);
                    break;
                default:
                    break;
            }
            return builder;
        }

    }

    protected static class DataAccumulator implements IDataArrayTarget {

        private final Map<String, Object> dataMap;

        public DataAccumulator() {
            dataMap = new ConcurrentHashMap<>();
        }

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public Map<String, Object> getData() {
            return dataMap;
        }

        @Override
        public void setData(Map<String, Object> data) {
            dataMap.clear();
            if (data != null) {
                dataMap.putAll(data);
            }
        }

        @Override
        public void addData(Map<String, Object> dataToAdd) {
            if (dataToAdd != null) {
                dataMap.putAll(dataToAdd);
            }
        }

        @Override
        public void removeData(Collection<String> idsToRemove) {
            if (idsToRemove != null) {
                for (String id : idsToRemove) {
                    if (id != null) {
                        dataMap.remove(id);
                    }
                }
            }
        }

        public void clearData() {
            dataMap.clear();
        }

    }

    private class AttributeGroupListener implements IRefreshingGroupListener {

        @Override
        public void groupRefreshed(GroupEvent event) {
            if (event != null && CometeTrendController.REFESHING_GROUP.equals(event.getGroupName())) {
                chart.setData(dataAccumulator.getData());
            }
        }

    }

}
